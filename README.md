# Epee Engine is a Open Source Cross Platform 2d Graphics Engine using SDL Written in C++ #
 

###License###
Epee Engine is licensed under the [Zlib license]( http://opensource.org/licenses/Zlib)


The Logging code for this engine has spawned a new logging lib the [EELog](https://bitbucket.org/AlanU/eelog)


Currently this project is on the back burner as I am focusing most of my energy on [FreeStick](https://bitbucket.org/freestick/freestick)