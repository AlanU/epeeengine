
/* Simple program:  Create a blank window, wait for keypress, quit.

   Please see the SDL documentation for details on using the SDL API:
   /Developer/Documentation/SDL/docs.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "EpeeEngineCommenIncludes.h"

#include "EpeeEngine.h"
#include "EpeeUtility.h"
#include "EELog.h"
#include <math.h>
#include <time.h>
const unsigned int Maxnum=100;
const unsigned int LeftNumberMax=12;
const unsigned int RightNumberMax=12;
#pragma warning(disable : 4996)
std::string TranslateOppToString(unsigned int _opp)
{
	switch(_opp)
	{
		case 0:
			return "+";
            break;
		case 1:
			return "-";
            break;
		case 2:
			return "*";
            break;
	}
	return "+";
}

int EvaluateQuestions(int _var1,int _var2,unsigned int _opp)
{

	switch(_opp)
	{
		case 0:
			return _var1+_var2;
            break;
		case 1:
			return _var1-_var2;
            break;
		case 2:
			return _var1*_var2;
            break;

	}
	return _var1+_var2;
}


enum State
{
	Restart=0,
	DisplayQuestion,
	WaitForAnswer,
	EvaluateAnswer,
	CheckForMoreQuestions,
	Done

};


int main (int argc, char *argv[])
{
    EpeeEngine GraphicsEngine;
    GraphicsEngine.SetUp(800,600,false,"Math Questions",false,60,32);
    textBox * pMessage=GraphicsEngine.CreateTextBox("message",100,50,5,"1234","Current",-1,-1,True_Type_Font_Defalut,18,255,255,255,True_Type_Font_Location);
    textBox * pVar1=GraphicsEngine.CreateTextBox("var1",100,100,5,"1234","Current",-1,-1,True_Type_Font_Defalut,18,255,255,255,True_Type_Font_Location);
    textBox * pVar2=GraphicsEngine.CreateTextBox("var2",200,100,5,"1234","Current",-1,-1,True_Type_Font_Defalut,18,255,255,255,True_Type_Font_Location);
    textBox * pOperatorText=GraphicsEngine.CreateTextBox("operator",150,100,5,"-","Current",-1,-1,True_Type_Font_Defalut,25,255,0,0,True_Type_Font_Location);
    textBox * pAnswer=GraphicsEngine.CreateTextBox("Answer",120,150,3,"Click and Type Answer Here","Current",-1,-1,True_Type_Font_Defalut,18,255,255,255,True_Type_Font_Location);
    if(!(pVar1 && pVar2 && pOperatorText && pAnswer&& pMessage))
    {

        EE_ERROR<<"could not create textbox see stderr for details"<<std::endl;
        return 0;
    }
    srand ((unsigned int ) time(NULL) );

    pAnswer->SetBackGroundColor(0,255,0);
    pAnswer->SetColorChangedOnClick(true);
    pAnswer->SetQuality(FONT_QUALITY_SHADED);
    pAnswer->SetSelectBackGroundColor(255,0,0);
    pVar1->SetQuality(FONT_QUALITY_HIGH);
    pVar1->SetTextJustification(TEXT_RIGHT_JUSTIFIED);
    pVar2->SetQuality(FONT_QUALITY_HIGH);
    pVar2->SetTextJustification(TEXT_LEFT_JUSTIFIED);
    pOperatorText->SetQuality(FONT_QUALITY_HIGH);
    pOperatorText->SetTextJustification(TEXT_CENTER_JUSTIFIED);
    pMessage->SetQuality(FONT_QUALITY_HIGH);
    pAnswer->SetNumbersOnly(true);


    unsigned int uiCurrentQuestion=1;
    unsigned int uiNumberCorrect=0;



    State CurrentState=DisplayQuestion;
    int iLeftSide=0;
    int iRightSide=0;
    unsigned int uiOpp=0;
    int iAnswer=0;
    std::string message=" ";
    std::string temp="";




    SDL_Event event;
    while (1)
    {

        while(GraphicsEngine.GetEvent(&event))
        {


            switch(event.type)
            {
                case SDL_QUIT:
                    return 0;
                    break;
                case  SDL_KEYUP:
                    switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        return 0;
                        break;
                    case SDLK_KP_ENTER:
                    case SDLK_RETURN:
                        pAnswer->SetEditable(false);
                        CurrentState=EvaluateAnswer;
                        break;
                    case SDLK_r:
                        if(CurrentState==Done)
                        {
                            CurrentState=Restart;
                        }
                        break;
                    default:
                    break;


                }

                    break;
                default:
                break;
            }//end of  switch(event.type)

        }//end of while(GraphicsEngine.GetEvent(&event))



	 	switch (CurrentState)
		{
            case Restart:
                uiNumberCorrect=0;
                uiCurrentQuestion=1;
                CurrentState=DisplayQuestion;
                break;

            case DisplayQuestion:
                iLeftSide=rand()%LeftNumberMax;
                iRightSide=rand()%RightNumberMax;
                uiOpp=rand()%3;
                pVar1->SetTextFromInt(iLeftSide);
                pVar2->SetTextFromInt(iRightSide);
                pOperatorText->SetText(TranslateOppToString(uiOpp));
                CurrentState=WaitForAnswer;
                break;

            case WaitForAnswer:
                pAnswer->SetEditable(true);
                pMessage->SetText("What is the Answer?");
                break;

            case EvaluateAnswer:
                if(pAnswer->GetTextToInt(iAnswer))
                {
                    if(EvaluateQuestions(iLeftSide,iRightSide,uiOpp)==iAnswer)
                    {
                        pMessage->SetText("That is Correct");
                        uiNumberCorrect++;

                    }
                    else
                    {
                        pMessage->SetText("That is Incorrect");

                    }



                    CurrentState=CheckForMoreQuestions;

                }
                else
                {

                    pAnswer->SetText("Invalid input");
                    CurrentState=WaitForAnswer;



                }
                break;

            case CheckForMoreQuestions:
                SDL_Delay(1000);
                if(uiCurrentQuestion>9)
                {
                    temp="";
                    if(EpeeUtility::StringFromNumericData(temp,uiNumberCorrect))
                    {
                        message="You Got "+temp+" Out of "+"10 "+"Correct";
                    }
                    pMessage->SetText(message);
                    CurrentState=Done;

                }
                else
                {
                    uiCurrentQuestion++;
                    CurrentState=DisplayQuestion;
                }

                break;
            case Done:
                break;


		}//end of switch logic


        GraphicsEngine.RenderSeen(true);

    }//end of while (1)

    return 0;
}
