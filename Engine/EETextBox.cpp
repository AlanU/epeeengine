/*
*  EETextBox.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EETextBox.h"
#include "EELog.h"
//**************************Begin Of textBox Class Funions*****************


textBox::textBox(int _Obj_Type,const std::string & _NameTextBox,int _x,int _y,int _z,const std::string & _TextBoxMessage,unsigned int _height,unsigned int _width,const std::string & _Font,int _FontPoint,unsigned int _red,unsigned int _blue ,unsigned int _green,const std::string & _FontPath):EEWidget(_NameTextBox,_Obj_Type)
{

	m_sTextBoxMessage=_TextBoxMessage;
	SetLocation(_x,_y);

	SetLocationZ(_z);
	// m_sFont=_Font;
	m_bChangeTextBoxColorOnClick=false;
	m_bQuality=FONT_QUALITY_LOW;
	m_iFontPoint=_FontPoint;

	m_cColorOfText.r=_red;
	m_cColorOfText.b=_blue;
	m_cColorOfText.g=_green;

	m_cBackGroundColor.r=255-_red;
	m_cBackGroundColor.g=255-_green;
	m_cBackGroundColor.b=255-_blue;

	m_cColorOfSelectedText.r=255;
	m_cColorOfSelectedText.g=255;
	m_cColorOfSelectedText.b=255;

	m_cSelectedBackGroundColor.r=0;
	m_cSelectedBackGroundColor.g=0;
	m_cSelectedBackGroundColor.b=255;

	m_sTrue_Type_Font_Location=_FontPath;//True_Type_Font_Location  is defined at the top of the file
	m_bblit=true;
	m_pLoadedFont=NULL;
	m_bEditable=false;
	m_bNumbersOnly=false;
	m_iJustify=TEXT_LEFT_JUSTIFIED;
	m_iOffsetXJustiy=0;
	//m_pLoadedFont=
	this->SetFont(_Font);
	SetHeightWidth(_height,_width);
	m_cModBox.h=this->GetHeight();
	m_cModBox.w=this->GetWidth();
	m_cModBox.x=_x;
	m_cModBox.y=_y;

}

textBox::textBox(const std::string & _NameTextBox, int _x, int _y,int _z,const std::string & _TextBoxMessage,unsigned int _height,unsigned int _width,const std::string & _Font,int _FontPoint,unsigned int _red,unsigned int _blue ,unsigned int _green,const std::string & _FontPath):EEWidget(_NameTextBox,Type_TextBox)
{

	m_sTextBoxMessage=_TextBoxMessage;
	SetLocation(_x,_y);

	SetLocationZ(_z);
	m_bChangeTextBoxColorOnClick=false;
	//m_sFont=_Font;
	m_bQuality=FONT_QUALITY_LOW;
	m_iFontPoint=_FontPoint;

	m_cColorOfText.r=_red;
	m_cColorOfText.b=_blue;
	m_cColorOfText.g=_green;

	m_cBackGroundColor.r=255-_red;
	m_cBackGroundColor.g=255-_green;
	m_cBackGroundColor.b=255-_blue;

	m_cColorOfSelectedText.r=255;
	m_cColorOfSelectedText.g=255;
	m_cColorOfSelectedText.b=255;

	m_cSelectedBackGroundColor.r=0;
	m_cSelectedBackGroundColor.g=0;
	m_cSelectedBackGroundColor.b=255;

	m_sTrue_Type_Font_Location= _FontPath;//True_Type_Font_Location  is defined at the top of the file
	m_bblit=true;
	m_pLoadedFont=NULL;
	m_bEditable=false;
	m_bNumbersOnly=false;
	m_iJustify=TEXT_LEFT_JUSTIFIED;
	m_iOffsetXJustiy=0;
	//m_pLoadedFont=
	this->SetFont(_Font);
	this->SetHeightWidth(_height,_width);

}

void  textBox::Render()
{
	if(GetBlit())
	{
		if(LoadSurface())
		{
			LoadTexture();
		}

	}
	EEWidget::Render();
	if(m_GLTexture==0)
		SetBlit(true);
}
/*
void  textBox::SDL_GL_SurfaceToTexture(SDL_Surface * surface)
{

//Refine , Rewrite it and make it my own
if(m_GLTexture!=0)
{
glDeleteTextures( 1, &m_GLTexture);
m_iTextureH=0;
m_iTextureW=0;
m_GLTexture=0;

}
int h=NearestPowerOfTwo(surface->h);
int w=NearestPowerOfTwo(surface->w);

SDL_Surface *image;
SDL_Rect area;
Uint32 saved_flags;
Uint8  saved_alpha;
image = SDL_CreateRGBSurface(
SDL_SWSURFACE,
w, h,
32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
0x000000FF,
0x0000FF00,
0x00FF0000,
0xFF000000
#else
0xFF000000,
0x00FF0000,
0x0000FF00,
0x000000FF
#endif
);
if(m_pModifySurface)
{
SDL_FreeSurface(m_pModifySurface);
m_pModifySurface=NULL;
}

saved_flags = surface->flags&(SDL_SRCALPHA|SDL_RLEACCELOK);
saved_alpha = surface->format->alpha;
if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
SDL_SetAlpha(surface, 0, 0);
}


area.x = 0;
area.y = 0;
area.w = surface->w;
area.h = surface->h;
SDL_BlitSurface(surface, &area, image, &area);


if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
SDL_SetAlpha(surface, saved_flags, saved_alpha);
}


//m_pModifySurface=rotozoomSurfaceXY(	tempalpha,270,1,-1,0);
CreateTexture(image,surface);

//SDL_UnlockSurface(temp);
SDL_FreeSurface(image);

//SDL_FreeSurface(tempalpha);
//return texture;
}


void textBox::CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image)
{
int h=NearestPowerOfTwo(_image->h);
int w=NearestPowerOfTwo(_image->w);
if(_baseSurface && _image)
{
glGenTextures(1, &m_GLTexture);
glBindTexture(GL_TEXTURE_2D, m_GLTexture);
//SDL_LockSurface(temp);


glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_NEAREST);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, _baseSurface->pixels);
//	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,_image->w,_image->h,GL_RGBA,GL_UNSIGNED_BYTE,_image->pixels);
m_iTextureH=h;
m_iTextureW=w;
}
return;
}*/

void textBox::SetModifySurFace(SDL_Surface * _ModifySurface)
{
	if(_ModifySurface)
	{
		if(m_pModifySurface)
		{
			SDL_FreeSurface(m_pModifySurface);
		}
		m_pModifySurface=_ModifySurface;
		//SetHeightWidth(m_pModifySurface->h,m_pModifySurface->w);
		m_cModBox.h=m_pModifySurface->h;
		m_cModBox.w=m_pModifySurface->w;
	}

}

textBox::textBox()
{
}

textBox::~textBox()
{
	if(m_pLoadedFont)
	{
		TTF_CloseFont(m_pLoadedFont);
	}

}
bool textBox::WillTextFitIntoBox(std::string _text)
{
	int w=0;
	int h=0;
	if(m_pLoadedFont)
	{
		TTF_SizeText(m_pLoadedFont,_text.c_str(), &w, &h);
		if(w<=this->m_cBox.w)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	return false;
}

EEWidget * textBox::WasClicked(int _mouselocationX, int _mouselocationY)
{
	bool CurrentSelectedBeforeSuper=this->m_bCurrentlySelected;
	EEWidget * ReturnSprocket=NULL;
	ReturnSprocket=NULL;//EEWidget::WasClicked(_mouselocationX,_mouselocationY);

	if(this->GetModifySurFace())
	{
		if(m_bactive )
		{


			if( ( _mouselocationX >  m_cBox.x ) && ( _mouselocationX <  m_cBox.x +  m_cModBox.w ) && (_mouselocationY >  m_cBox.y ) && (_mouselocationY < m_cBox.y + m_cModBox.h ) )
			{
				this->m_bCurrentlySelected=true;
				ReturnSprocket=this;
			}

		}

		this->m_bCurrentlySelected=false;
	}
	else
	{
		ReturnSprocket=EEWidget::WasClicked(_mouselocationX,_mouselocationY);
	}




	if(this->m_bCurrentlySelected!=CurrentSelectedBeforeSuper)
	{
		this->SetBlit(true);
	}
	return ReturnSprocket;

}
void textBox::SetSelectBackGroundColor(unsigned int _red,unsigned int _blue ,unsigned int _green)
{
	m_cSelectedBackGroundColor.r=_red;
	m_cSelectedBackGroundColor.g=_green;
	m_cSelectedBackGroundColor.b=_blue;

}
void textBox::SetSelectTextColor(unsigned int _red,unsigned int _blue ,unsigned int _green)
{

	m_cColorOfSelectedText.r=_red;
	m_cColorOfSelectedText.g=_green;
	m_cColorOfSelectedText.b=_blue;


}
bool textBox::IsColorChangedOnClick()
{
	return m_bChangeTextBoxColorOnClick;
}

void textBox::SetColorChangedOnClick(bool _ChangeOnClick)
{
	m_bChangeTextBoxColorOnClick=_ChangeOnClick;
}

bool textBox::GetClipTextHeight()
{
	return m_bDoNotClipTextHeight;
}

bool textBox::GetClipTextWidth()
{
	return m_bDoNotClipTextWidth;
}

void textBox::SetQuality(int _Quality)
{
	if(_Quality!=m_bQuality && _Quality>=FONT_QUALITY_LOW && _Quality<=FONT_QUALITY_SHADED )
	{
		m_bQuality=_Quality;
		this->SetBlit(true);
	}
}

int textBox::GetQuality()
{
	return m_bQuality;

}

void textBox::SetText(const std::string & _text,bool _setevent)
{
	if (m_sTextBoxMessage!=_text)
	{
		SetBlit(true);
	}

	m_sTextBoxMessage=_text;
	if(GetBlit())
	{
		if(m_bDoNotClipTextHeight || m_bDoNotClipTextWidth)
		{
			if(m_bDoNotClipTextHeight )
			{
				SetHightBasedOfRenderedText();

			}
			if(m_bDoNotClipTextWidth)
			{
				SetWidthBasedOfRenderedText();

			}
		}
	}

	if (_setevent) {

		SDL_Event event;

		event.type = SDL_USEREVENT;
		event.user.code = TEXT_BOX_CHANGED_EVENT;
		event.user.data1 = this;
		event.user.data2 = 0;
		SDL_PushEvent(&event);
	}
	SetJustifyOffset();

}

void textBox::SetHeightWidth(unsigned int _Height,unsigned int _Width)
{

	if (_Height==-1 || _Width==-1)
	{
		if(_Height==-1)
		{
			SetHightBasedOfRenderedText();
			_Height=this->GetHeight();
			m_bDoNotClipTextHeight=true;
		}
		else
		{
			m_bDoNotClipTextHeight=false;
		}
		if(_Width==-1)
		{
			SetWidthBasedOfRenderedText();
			_Width=this->GetWidth();
			m_bDoNotClipTextWidth=true;
		}
		else
		{
			m_bDoNotClipTextWidth=false;
		}




	}
	else
	{
		m_bDoNotClipTextWidth=false;
		m_bDoNotClipTextHeight=false;

	}

	EEWidget::SetHeightWidth(_Height,_Width);




}

void textBox::SetHightBasedOfRenderedText()
{
	int w=0,h=0;
	if (m_pLoadedFont) {

		if (TTF_SizeText(m_pLoadedFont,m_sTextBoxMessage.c_str(),&w,&h)) {
			return;
		}

		EEWidget::SetHeightWidth(h,this->GetWidth());
	}

}


void textBox::SetWidthBasedOfRenderedText()
{
	int w=0,h=0;
	if (m_pLoadedFont) {
		if (TTF_SizeText(m_pLoadedFont,m_sTextBoxMessage.c_str(),&w,&h)) {
			return;
		}

		EEWidget::SetHeightWidth(this->GetHeight(),w);

	}
}


int textBox::GetJustifyOffset()
{
	return m_iOffsetXJustiy;
}

int textBox::GetTextJustification()
{
	return m_iJustify;
}

void textBox::SetTextJustification(int _justify)
{
	if (_justify==TEXT_LEFT_JUSTIFIED || _justify==TEXT_RIGHT_JUSTIFIED || _justify ==TEXT_CENTER_JUSTIFIED) {
		m_iJustify=_justify;
		SetJustifyOffset();

	}
}

std::string textBox::GetText ()
{
	return m_sTextBoxMessage;
}

bool textBox::GetTextToInt(int & _value)
{
    return GetTextToNumericData(_value);
}

bool textBox::GetTextToFloat(float & _value)
{
    return GetTextToNumericData(_value);
}

bool  textBox::SetTextFromInt(int _value,bool _setevent)
{
	return SetTextFromNumericData( _value, _setevent);
}

bool textBox::SetTextFromFloat(float _value,bool _setevent)
{
	return SetTextFromNumericData( _value, _setevent);
}


std::string textBox::GetFont()
{
	return m_sFont;
}

void textBox::SetFont(const std::string & _FontArg)
{
	if(m_sFont!=_FontArg)
	{

		m_sFont=_FontArg;
		LoadFont();
	}
}

int textBox::GetFontPoint()
{
	return m_iFontPoint;
}

void textBox::SetFontPoint(int _FontPointArg)
{	if(m_iFontPoint!=_FontPointArg)
{
	m_iFontPoint=_FontPointArg;
	LoadFont();
}
}

SDL_Color textBox::GetTextColor()
{
	return m_cColorOfText;
}

bool textBox::SetTextColor(unsigned int _red,unsigned int _blue ,unsigned int _green)
{
	if(m_cColorOfText.r!=_red || m_cColorOfText.b!=_blue || m_cColorOfText.g!=_green)
	{
		m_cColorOfText.r=_red;
		m_cColorOfText.b=_blue;
		m_cColorOfText.g=_green;
		SetBlit(true);;
	}

	return true;
}
SDL_Color textBox::GetBackGroundColor()
{
	return m_cBackGroundColor;
}
bool textBox::SetBackGroundColor(unsigned int _red,unsigned int _blue ,unsigned int _green)
{
	if(m_cBackGroundColor.r!=_red ||m_cBackGroundColor.b!=_blue || m_cBackGroundColor.g!=_green)
	{
		m_cBackGroundColor.r=_red;
		m_cBackGroundColor.b=_blue;
		m_cBackGroundColor.g=_green;
		SetBlit(true);;
	}

	return true;
}

std::string textBox::GetFontPath()
{
	return m_sTrue_Type_Font_Location;
}

void textBox::SetFontPath(const std::string & _Location)
{
	m_sTrue_Type_Font_Location=_Location;
	LoadFont();
}

void textBox::SetEditable(bool _flag)
{
	m_bEditable=_flag;
	m_bDoNotClipTextHeight=false;
	m_bDoNotClipTextWidth=false;

}

bool textBox::GetEditable()
{
	return m_bEditable;
}

void textBox::SetNumbersOnly(bool _flag)
{
	m_bNumbersOnly=_flag;

}

bool textBox::GetNumbersOnly()
{
	return m_bNumbersOnly;
}


TTF_Font * textBox::GetFontPointer()
{
	return m_pLoadedFont;
}



SDL_Surface * textBox::LoadSurface()
{
	if (m_pLoadedFont!=NULL)
	{


		if(m_porignalSurface)
		{
			SDL_FreeSurface(m_porignalSurface);
		}
		if(this->m_bCurrentlySelected && this->m_bChangeTextBoxColorOnClick)
		{

			m_porignalSurface=TTF_RenderText_Shaded( m_pLoadedFont,m_sTextBoxMessage.c_str(),m_cColorOfSelectedText,m_cSelectedBackGroundColor);

		}
		else
		{

			switch (m_bQuality)
			{
			case FONT_QUALITY_HIGH :
				m_porignalSurface=TTF_RenderText_Blended( m_pLoadedFont,m_sTextBoxMessage.c_str(),m_cColorOfText);
				break;
			case FONT_QUALITY_LOW:
				m_porignalSurface=TTF_RenderText_Solid( m_pLoadedFont,m_sTextBoxMessage.c_str(),m_cColorOfText);
				break;
			case FONT_QUALITY_SHADED:
				m_porignalSurface=TTF_RenderText_Shaded( m_pLoadedFont,m_sTextBoxMessage.c_str(),m_cColorOfText,m_cBackGroundColor);
				break;
			default:
				m_porignalSurface=TTF_RenderText_Solid( m_pLoadedFont,m_sTextBoxMessage.c_str(),m_cColorOfText);
				break;
			}
		}


		if(m_porignalSurface)
		{
			if(m_pModifySurface)
			{
				SDL_FreeSurface(m_pModifySurface);
			}
			//LoadTexture();

			SetBlit(false);
		}
		else
			SetBlit(true);

		return    m_porignalSurface;
	}
	else
	{
		return NULL;
	}
	return NULL;
}

void textBox::LoadFont()
{
	std::string FontToLoad=m_sTrue_Type_Font_Location+ m_sFont;
	if(m_pLoadedFont)
	{
		TTF_CloseFont(m_pLoadedFont);
		m_pLoadedFont=TTF_OpenFont(FontToLoad.c_str(),m_iFontPoint);
	}
	else
	{
		m_pLoadedFont=TTF_OpenFont(FontToLoad.c_str(),m_iFontPoint);
	}


	if(!m_pLoadedFont)
	{
		EE_ERROR<<"could not load font "<<FontToLoad.c_str()<<" for Sprocket "<<GetName()<<std::endl;
	}

	SetBlit(true);

}

void textBox::SetJustifyOffset()
{

	std::string tempforJustiying="";
	int w=0,h=0;
	if (m_iJustify!=TEXT_LEFT_JUSTIFIED && m_pLoadedFont!=NULL) {


		if (TTF_SizeText(m_pLoadedFont,m_sTextBoxMessage.c_str(),&w,&h)) {
			return;
		}
		switch(m_iJustify) {
		case TEXT_RIGHT_JUSTIFIED:
			m_iOffsetXJustiy=this->GetWidth()-w;
			break;
		case TEXT_CENTER_JUSTIFIED:
			m_iOffsetXJustiy=this->GetWidth()-w;
			m_iOffsetXJustiy=m_iOffsetXJustiy/2;
			break;

		}
	}

}


//**************************End of texBox Class Funtions*****************************
