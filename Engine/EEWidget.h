/*
*  EEWidget.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EEWIDGET_H
#define EEWIDGET_H


#include "EpeeEngineError.h"

#include "EpeeEngineCommenIncludes.h"

//***********************Begin Of Sprocket Class***************************
class EEWidget //Base Class
{
protected:
	int m_iz;
	std::string m_sName;
	int m_iSprocketType;
	bool m_bDraw;
	int m_iCurrentPostion;
	bool m_iAntiAliased;
	bool m_bblit;
	bool m_bShowSurfaceOutline;


protected:
	double m_iRotation;
	double m_iScaleX;
	double m_iScaleY;
	bool m_bactive;
	bool  m_bDontMoveButton;
	bool m_bCurrentlySelected;
	SDL_Rect m_cBox;
	unsigned int m_iTextureH;
	unsigned int m_iTextureW;
	SDL_Surface *m_porignalSurface;
	SDL_Surface *m_pModifySurface;
	GLuint m_GLTexture;
	virtual void Render();
	EEWidget();

public:
	EEWidget(std::string _NameMain,int _TypeMain);
	EEWidget(std::string _NameMain,int _TypeMain, int _x, int _y,int _z);

	virtual ~EEWidget();
	virtual void Init();
	friend class EpeeEngine;
	friend class RenderList;
	virtual void LoadTexture();
	virtual void UnloadSurface();
	virtual SDL_Surface * LoadSurface();
	virtual void UnLoadTexture();
	virtual bool IsLoaded();
	virtual void SDL_GL_SurfaceToTexture(SDL_Surface * surface);
	virtual void CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image);
	int NearestPowerOfTwo(int i);
	void SetLocation(int _x,int _y);
	int GetLocationX();
	int GetLocationY();
	std::string GetName();
	int GetLocationZ ();
	virtual void SetHeightWidth(unsigned int _Height,unsigned int _Width);
	unsigned int GetHeight();
	unsigned int GetWidth ();
	bool SetDraw(bool _Draw);
	bool GetDraw();
	int GetType();
	bool SetScalingFactor(double _ScalingX,double _ScalingY);
	bool SetScalingFactorY(double _ScalingY);
	bool SetScalingFactorX(double _ScalingX);
	double  GetScalingFactorX();
	double  GetScalingFactorY();
	bool SetRotation(double _Degrees );
	double GetRotation();
	bool SetAntiAliased(bool _AntiAliased );
	bool GetAntiAliased();
	int GetCurrentVectorPostion();
	virtual EEWidget * WasClicked(int _mouselocationX, int _mouselocationY);
	void DisableClick();
	void EnableClick();
	bool GetIsClickEnabled();
	void DisableAndTurnOffDraw();
	void EnableAndTurnOnDraw();
	void SetDontMoveOnClick(bool _flag);
	bool GetDontMoveOnClick();
	bool WasLastSprocketClicked();
	void ShowOutLine();
	void HideOutLine();
	bool GetOutLine();
protected:

	bool GetBlit();
	virtual SDL_Surface * SetSurface();
	virtual SDL_Surface * GetEEWidgeturface();
	SDL_Rect * GetRect();
	bool SetCurrentArrayPostion(int _newPostion);
	void SetType(int _type);
	void SetLocationZ(int _z);//DO NOT call this function call ChangeSprocketZ instead
	void SetBlit(bool _blit);
	virtual void SetModifySurFace(SDL_Surface * _ModifySurface);
	SDL_Surface * GetModifySurFace();


};
//***********************End Of Sprocket Class*******************************

#endif // EEWIDGET_H
