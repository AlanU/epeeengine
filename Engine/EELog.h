//
//  EELog.h
//  Example
//
//  Created by Alan Uthoff on 5/19/12.
//  Copyright (c) 2012. All rights reserved.
//

#ifndef Example_EELog_h
#define Example_EELog_h
#include "EEStream.h"
#include <ostream>
#include <sstream>

//TODO add NULL stream for disabling a level
enum EELogLevels
{
	EE_LEVEL_INFO = 0,
	EE_LEVEL_WARNING,
	EE_LEVEL_ERROR,
	EE_LEVEL_DEBUG,
	EE_LEVEL_LAST,
};

#define EE_INFO	EELog::GetInstance()->GetLogLevel(EE_LEVEL_INFO)
#define EE_WARNING EELog::GetInstance()->GetLogLevel(EE_LEVEL_WARNING)
#define EE_ERROR  EELog::GetInstance()->GetLogLevel(EE_LEVEL_ERROR)
#define EE_DEBUG EELog::GetInstance()->GetLogLevel(EE_LEVEL_DEBUG)

class EELog
{

public:
	static EELog * GetInstance();
	static void Destroy();
	EEStream & GetLogLevel(EELogLevels _Level);

protected:
	EEStream * m_StreamLevels[EE_LEVEL_LAST];
private:
	static EELog * EELogInstance;
	EELog(){};
	~EELog(){};
	EELog(const EELog &){};
	EELog & operator = (const EELog &)
	{
		return *this;
	}
	void Init();
	void TearDown();
	void PrependLevelInformation(EELogLevels _Level);

};

#endif
