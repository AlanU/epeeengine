/*
*  EEAnimationScript.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EEANIMATIONSCRIPT_H
#define EEANIMATIONSCRIPT_H
#include "EpeeEngineCommenIncludes.h"
#include "EEWidget.h"

//********************Begin Of Animation Sctipt Class****************************
typedef bool (*PlayScrtipFuntionPointer) (float,EEWidget *);
class AnimationScript : public EEWidget
{
private:
	int m_ianimationFPS;
	float m_iFrameEverInMillSecounds;
	int m_iNumFrames;
	int m_iLoop;
	int m_iCurrentFrame;
	float m_iCurrentTime;
	float m_iPrevTime;
	bool m_bPlaying;
	int m_iNumberofTimesPlayed;
	int m_iframeEvent;
	bool m_bReversPlay;
	bool 	m_bFuntionIsDoneWithOneCycle;
	PlayScrtipFuntionPointer* m_fPlayScrtipFuntionPointer;
	EEWidget * m_pAnimationSprocket;

public:
	AnimationScript(const std::string & _AnimationName,EEWidget * _AnimationSprocket,int _z,PlayScrtipFuntionPointer* _ScrtipFuntionPointer,int _fps,int _TotalNumberOfFrames,int _StartFrame=0,int _Loop=1,bool _StartNow=true);//:image( _FileName, _AnimationName, _x, _y, _z,Type_Animation,_Transparency)

	void  JumpToFrame(int _Frame);
	void UpdateTime();
	int GetFPS();
	void SetFPS(int _fps);
	void StopAnimation();
	void PlayAnimation();
	void ResumeAnimation();
	void SetLoop(int _Loop);
	int GetLoop();
	int GetCurrentFrame();
	int GetTotalNumberOfFrames();
	int GetNumberOfTimesPlayed();
	void SetFrameEvent(int _frame);
	int GetFrameEvent();
	bool IsPlaying();
	void SetFuntionPointer(PlayScrtipFuntionPointer * _PlayScrtipFuntionPointer);
	PlayScrtipFuntionPointer GetFuntionPointer();
	virtual bool Script(float _time,EEWidget * _AnimationSprocket);//called if m_fPlayScrtipFuntionPointer is NULL
	//virtual bool ResetFuntion(float _time,EEWidget * _AnimationSprocket);//called to reset the script
private:
	void CheckForNextFrame(float _time)	;
};
//*******************End of Animation Script Class*********************

#endif // EEANIMATIONSCRIPT_H
