/*
*  EpeeEngine.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/


#ifndef _EPEEENGINE_h_
#define _EPEEENGINE_h_


#define Version_Number_h .40
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <sstream>
#include "tinyxml.h"
#include "EpeeEngineCommenIncludes.h"
#include "EEWidget.h"
#include "EpeeEngineError.h"
#include "EETextBox.h"
#include "EESceneGraph.h"
#include "EEImage.h"
#include "EEAnimation.h"
#include "EEAnimationScript.h"
#include "EESound.h"
#include "EEButton.h"
#include "EETextButton.h"


//********************Begin Of ControlCharacters Class******************************
class ControlCharacters
{
	bool m_bShift;
	bool m_bControl;
	bool m_bAlt;
	bool m_bCommand;
	bool m_bCapsLock;
	bool m_keyCaught;


public:
	friend class EpeeEngine;
	ControlCharacters();
	bool IsAltPressed();
	bool IsCtrlPressed();
	bool IsShiftPressed();
	bool IsCapLockPressed();
protected:
	void ProcessKeyEvent(SDL_Event *_event);
	void SetCapLock(bool _caps);
	bool GetKeyCaught();


};
//***********************End Of ControlCharacters Class*********************************


//***********************Begin Of FileLookUp Class***************************
class FileLookUp
{

public:
	FileLookUp();
	FileLookUp(const std::string & _Symbol, const std::string & _fileName);
	std::string m_ssymbol;
	std::string m_sfilename;


};
//***********************End Of FileLookUp Class***************************

class EpeeEngine
{


public:
	EpeeEngine();
	EpeeEngine(std::vector<FileLookUp> filetable);
	~EpeeEngine();


	bool EventProcessing(SDL_Event *_event); //event processing for the graphics engin currently handles movement of buttons when pressed
	bool SetUp(unsigned int Screen_Width,unsigned int Screen_Heigth,bool FullSreenMod=false,std::string  _WindowName="Your App Name Here",bool _Resize=false,unsigned int _fps=60,unsigned int _BitDepthOfScreen=16,bool Anyformat=false);
	bool SetUp(std::vector<FileLookUp> filetable,unsigned int Screen_Width,unsigned int Screen_Heigth,bool FullSreenMode=false,const std::string &  _WindowName="Your App Name Here",bool _Resize=false,unsigned int _fps=60,unsigned int _BitDepthOfScreen=16,bool Anyformat=false);
	bool RenderSeen(bool _UpdateScreenNow=false); //call this to proccess the render list and pass true to this funtion for it to call updatescreen
	bool UpdateScreen();// draws the proccesed render list to the screen

	image * CreateImageFromList(const std::string &  _symbol,const std::string &  _ImageName,int _x,int _y,int _z);//creates a new image from a filelist and adds it to renable list
	bool RemoveIteamFromRendableList(const std::string &  _card); //removes the card from the rendable list

	bool SetNumberOfMixingChannels(int _NumberOfChannels);//Set up funtion for the sound list this need to be at lest on for sounds to work. This does not have any barring to the background music
	Sound *  FindSound(const std::string &  _name);//returns a pointer to a sound object from the sound list
	Sound * CreateSound(const std::string &  _GroupName,const std::string &  _FileName,int _Channel=0,int _NumberOfTimesToLoop=0,int _Volume=MIX_MAX_VOLUME);//creates a new sound and adds it to the sound list
	bool DestroySound(const std::string &  _name);//deletes the a sound from the sound list
	bool PlaySoundNow(Sound * _pSound);//plays a sound pointed to by _pSound

	bool SetAndPlayBackGroundMusic(const std::string &  _NameOfAudio,int _NumberOfTimesToLoop,int _Volume=MIX_MAX_VOLUME,int _ms=0);//creates and plays background music. THIS IS NOT ADDED TO THE SOUND LIST. you can only have one music file loaded and playing at a time
	bool StopBackGroundMusic(int _ms=0);//stops the background music
	bool FadeInSound(Sound * _pSound,int _ms);
	bool FadeOutSound(Sound * _pSound,int _ms);
	bool FadeOutSound(int _Channel,int _ms);
	bool StopSoundNow(int _Channel);
	bool StopSoundNow(Sound * _pSound);
	bool IsSoundPlaying(Sound *_pSound);
	bool IsBackGoundMusicPlaying();

	bool LoadConfigurationFile(const std::string & _FileName);



	float GetTimeSinceSetup(); //gets the number of milliseconds since setup was called
	void SleepDelay(Uint32 _millisecounds);	//sleeps for _millisecounds does not use up clock cycles
	textBox * CreateTextBox(const std::string &  _Name,int _x,int _y,int _z,const std::string & _TextBoxMessage=" ",const std::string &  _AddtoCurrentRenderlist="Current",unsigned int _height=-1,unsigned int _width=-1,const std::string &  _Font=True_Type_Font_Defalut,int _FontPoint=18,unsigned int _red=255,unsigned int _blue=255 ,unsigned int _green=255,const std::string &  _FontPath = True_Type_Font_Location);//creates a textbox and adds it to the rendable list
	image * CreateImage(const std::string &  _fileName ,const std::string &  _Name,int _x,int _y,int _z,const std::string &   _AddtoCurrentRenderlist="Current",bool _Transparency=false);//creates image and adds it to the rendable list
	Animation * CreateAnimation(const std::string &  _fileName ,const std::string &  _Name,int _x,int _y,int _z,int _NumberOfFrames,int _fps,int _StartFrame=0,int _Loop=1,int _NumberOfRowsOfFrames=1,bool _Transparency=false,bool _StartNow=true,const std::string &   _AddtoCurrentRenderlist="Current");//Creates an animation and adds it to the renderable list
	AnimationScript * CreateAnimationScript(const std::string &  _Name,EEWidget * _AnimationSprocket,PlayScrtipFuntionPointer* _ScrtipFuntionPointer,int _z,int  _TotalNumberOfFrames,int _fps=30,int _StartFrame=0,int _Loop=1,bool _StartNow=true,const std::string &   _AddtoCurrentRenderlist="Current");

	ButtonTB * CreateButtonTB(const std::string &  _ButtonName,int _Buttonx,int _Buttony,int _Buttonz,const std::string &  _ButtonText ,bool _SButtonactive=true,const std::string &   _AddtoCurrentRenderlist="Current",unsigned int _ButtonTB_height=30,unsigned int _ButtonTB_width=100,const std::string &  _ButtonTB_Font=True_Type_Font_Defalut,int _ButtonTB_FontPoint=18,unsigned int _ButtonTB_red=255,unsigned int _ButtonTB_blue=255 ,unsigned int _ButtonTB_green=255,const std::string &  _ButtonFontPath=True_Type_Font_Location);//creates a textbox that is a button on the screen and adds the button to the rendable list
	Button * CreateButton(const std::string &  _localfileName ,const std::string &  _localName,int _localx,int _localy,int _localz,bool _Transparency=false,bool _localactive=true,const std::string &   _AddtoCurrentRenderlist="Current");//creates a button that is attached to an image. the button is added to the rendable list
	Button * CreateButton(const std::string &  _localName,int _localx,int _localy,int _localz,int _height,int _width,bool _Transparency=false,bool _localactive=true,const std::string &   _AddtoCurrentRenderlist="Current");//creates a button that is not attached to an image an is invsable to the user. this button is added to the rendable list but is not draw on the screen
	EEWidget * WasAButtonClicked(int _x, int _y);//returns the button that contanes the x and y values.

	textBox * FindTextBox(const std::string &  _name);//returns a pointer to a textBox _name from the rendable list
	Button * FindButton(const std::string &  _name);//returns a pointer to a Button _name from the rendable list
	ButtonTB * FindButtonTB(const std::string &  _name);////returns a pointer to a ButtonTB _name from the rendable list
	image *FindImage(const std::string &  _name);//returns a pointer to a Image _name from the rendable list
	Animation * FindAnimation(const std::string &  _name);//returns a pointer to an Animation _name from the rendable list

	image* CloneImage(image * _imageToClone,const std::string &  _nameOfImage,const std::string &  _RenderList="Current",int _x=-1,int _y=-1,int _z=-1);
	Animation* CloneAnimation(Animation * _imageToClone,const std::string &  _nameOfAnimation,const std::string &  _RenderList,int _x=-1,int _y=-1,int _z=-1 );
	textBox* CloneTextBox(textBox * _textBoxToClone,const std::string &  _nameOfImage,const std::string &  _RenderList="Current",int _x=-1,int _y=-1,int _z=-1 );
	//***********NOTE IMAGES,ANIMATIONS TEXTBOXES,AND BUTTONS ARE ALL EEWidget***********************
	bool DestroySprocket(const std::string &  _name); // removes a sprocket from the rendable list
	EEWidget * FindEEWidget(const std::string &  _name,const std::string &  _renderlist="Current"); //returns a pointer to a sprocket from the rendable list
	bool ChangeSprocketZ(EEWidget * _SprocketToChange,int _newZ,RenderList * _list=NULL);//changes the depth of a sprocket


	int GetFps(); // returns the current frames to a secound of the render engine
	bool SetFps(int _fps);//sets the frames to a secound of the render engine

	bool IsFoucsOnTextBox()
	{
		if (m_pCurrentEditedTextBox==NULL) {
			return false;
		}
		else
		{
			return true;
		}
		return false;

	}
	std::string GetCurrentRenderList();
	RenderList * GetCurrentRenderListptr();
	bool SetCurrentRenderList(const std::string &  _ListToChangeTo);
	bool InsertRenderList(RenderList * _newRenderList);

	bool DestroyRenderList(const std::string &  _name);
	bool RemoveIteamFromRendableList(const std::string &  _card,const std::string &  _renderList);
	RenderList*  FindRenderList(const std::string &  _name);
	RenderList * CreateRenderList(const std::string &  _name);
	unsigned int GetTotalRenderList();
	bool ValideVersionNumberOfCppWithHeader();
	double GetVersionNumberH();
	double GetVersionNumberCpp();
	void TransferSprocketToNewRenderList(RenderList * _sorceRenderList,RenderList * _destinationRenderList,EEWidget * _sprocketToTransfer);

	//std::string GetLastError() {return m_sCurrentError;}//Returns the last error from the last funtion called
	EpeeEngineError GetLastError() {return m_cLastError;}//Returns the last error from the last funtion called

	unsigned int GetTotalSprockesCreated();//returns that total number of EEWidget created
	unsigned int GetTotalSoundsCreated();//returns the total number of sounds created
	void WriteRenderListToFile();//Writes the contents of the render list out to the cout file. Only works in debug right now
	void WriteSoundListToFile();//Writes the contents of the sound list out to the cout file. Only works in debug right now
	void TurnOnFPS();//Turns on the Frames Per Second in the upper left hand corner
	void TurnOffFPS();//Turns Off the Frames Per Second in the upper left hand corner
	void ToggleFullScreen(bool _FullScreen);//Toggles the window from fullscreen to window and back true for fullscreen false for windowed
	void ChangedResolution(int _Width, int _height,bool _useCurrentFlags=true,bool _FullScreen=false,bool _Resize=false,bool _AnyFormat=false);//Changes the current window resolution
	bool GetEvent(SDL_Event * _event); //Removes and polulates _event with the next event in the event que. Returns false if the is no events in the que returns true otherwise


	unsigned int GetCurrentScreenHeight();
	unsigned int GetCurrentScreenWidth();


	bool IsKeyDisplayable(SDLKey _KeyToCheck);
	char IsKeyModifiedByOtherKeys(SDLKey _KeyToCheck);
	ControlCharacters * GetControlCharacters();

	void DrawOutlineWithRect(SDL_Surface * _SurfaceToBeOutLined,SDL_Rect *OutLine,Uint8 _red=255,Uint8 _blue=0,Uint8 _green=0,unsigned int _thickness=1 );
	void DrawOutline(SDL_Surface * _SurfaceToBeOutLined,unsigned int _x,unsigned int _y,Uint8 _red=255,Uint8 _blue=0,Uint8 _green=0,unsigned int _thickness=1 );

	//template <typename datatype> datatype StringToNumericData(std::string _stringtoconvert,datatype temp);//temp is the data type that you want back
	template <typename datatype> datatype StringToNumericData(const std::string &  _stringtoconvert,datatype temp)
	{
		datatype tempint=0;

		std::istringstream tempstream(_stringtoconvert);
		if (!(tempstream>>tempint)) {
			EE_DEBUG<<"Could not convert"+_stringtoconvert+"to the specified data type"<<std::endl;
			return 0;
		}
		this->ClearError();
		return tempint;
	}


	//template <typename datatype> std::string  StringFromNumericData(datatype _value);
	template <typename datatype> const std::string StringFromNumericData(datatype _value)
	{
		datatype tempint=_value;
		std::ostringstream tempstream;
		if (!(tempstream<<_value)) {

			std::string tempstring ="Could not convert numeric data to string";
			EpeeEngineError temperror(tempstring,TEXTBOX_CONVERION_ERROR,this);
			this->SetLastError(temperror);
			return "NULL";

		}
		this->ClearError();
		return tempstream.str();
	}

private:
	void ClearError();
	void SetLastError(EpeeEngineError _error);
	void LoadAnimationFromFile(TiXmlElement *element);
	void LoadButtonTBFromFile(TiXmlElement *element);
	void  LoadButtonFromFile(TiXmlElement *element);
	void LoadTextBoxFromFile(TiXmlElement *element);
	void LoadImageFromFile(TiXmlElement *element);
	//bool WriteText(std::string TextToWrite,int x,int y,int point=12,std::string font="arial.ttf",int text_box_height=30,int text_box_width=200);
	//bool WriteText(textBox* _TextBox);
	bool InsertIntoSprocketList(EEWidget * _newSprocket,RenderList * _List=NULL);
	//bool DrawImage(image* _ImageToDraw);
	bool ScaleImageDraw(image* _ImageToDraw);
	void fpsDelay();
	bool UpdateVectorlocateion();
	bool PlaySoundNow(const std::string &  _NameOfAudio,int _NumberOfTimesToLoop,int _Channel=-1);
	void HandleKeyBordEvents(SDL_Event *_event);
	void HadleKeyBordKeys(SDL_Event *_event,bool flagtemp);
	bool HandleMouseEvents(SDL_Event *_event);
	std::vector <FileLookUp> m_cfiletable;
	Uint32 m_u32VideoFlags;
	//std::vector  <textBox*> m_vTextBoxList;
	//std::vector <Button> m_vButtonList;
	//	std::vector  <EEWidget*> m_vEEWidgetList;
	std::vector <RenderList*> m_vRenderList;
	std::vector <Sound*> m_vSounds;
	SDL_Surface *m_pscreen;
	EpeeEngineError m_cLastError;
	textBox * m_pCurrentEditedTextBox;
	unsigned int m_iScreen_Heigth;
	unsigned int  m_iScreen_Width;
	//	TTF_Font *m_pfont1;
	RenderList * m_uiCurrentRenderList;
	bool m_bFullScreen;
	bool m_bResizeAble;
	unsigned int m_ifps;
	Mix_Music *m_pBackGroundMusic;
	std::string m_sCurrentError;
	Mix_Chunk * m_pSoundOne;
	unsigned int m_iframe;
	unsigned int m_ifpstime;
	textBox * FrameRate;
	float m_fCurrentFrameRate;
	EEWidget  * m_pLastSprocketClicked;
	RenderList * m_LastRenderListFound;
	EEWidget * m_pDownedButton;
	unsigned int m_uiTotalRenderListsCreated;
	unsigned int m_uiTotalSoundsCreated;
	unsigned int m_uiTotalConnectionsCreated;
	bool SetKeyColor(SDL_Surface * _SurfaceToKey);
	int m_bHardWareAccelration;//0 is software 1 is openGl soon 2 will be directX
	ControlCharacters m_cKeybordControlCharacters;


};





/*float GetTimeSinceSetup2()
{
return	SDL_GetTicks();
}*/
#endif
