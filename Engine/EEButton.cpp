/*
*  EEButton.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EEButton.h"
//************************Begin of Button Class Funtions******************************

Button::Button(const std::string & _ButtonFileNameImage,const std::string & _ButtonName,int _Buttonx,int _Buttony,int _Buttonz,bool _Transparency,bool _SButtonactive):image( _ButtonFileNameImage, _ButtonName, _Buttonx, _Buttony, _Buttonz,Type_Button,_Transparency)
{
	m_bactive=_SButtonactive;
	m_ibuttonheight=-1;
	m_ibuttonwidth=-1;
	m_ibuttonx= -1;
	m_ibuttony=-1;
	m_bDontMoveButton=false;
}

Button::Button(const std::string & _ButtonName,int _Buttonx,int _Buttony,int _Buttonz,int m_iButtonHeight,int m_iButtonwidth,bool _Transparency,bool _SButtonactive):image(" ", _ButtonName, _Buttonx, _Buttony, _Buttonz,Type_Button,_Transparency)
{
	m_bactive=_SButtonactive;
	m_ibuttonheight=m_iButtonHeight;
	m_ibuttonwidth=m_iButtonwidth;
	m_ibuttonx= _Buttonx;
	m_ibuttony=_Buttony;
	m_bDontMoveButton=false;
}

Button::Button()
{

}

EEWidget * Button::WasClicked(int _mouselocationX, int _mouselocationY)
{
	SDL_Rect *clipbox =GetRect();
	if(m_bactive)
	{

		if( m_ibuttonheight ==-1 &&  m_ibuttonwidth==-1)
		{

			if( ( _mouselocationX > clipbox->x ) && ( _mouselocationX < clipbox->x + clipbox->w ) && (_mouselocationY > clipbox->y ) && (_mouselocationY < clipbox->y + clipbox->h ) )
			{
				this->m_bCurrentlySelected=true;
				return this;
			}


		}

		else
		{


			if( ( _mouselocationX >  m_ibuttonx ) && ( _mouselocationX <  m_ibuttonx +  m_ibuttonwidth ) && (_mouselocationY >  m_ibuttony ) && (_mouselocationY <  m_ibuttony + m_ibuttonheight ) )
			{
				this->m_bCurrentlySelected=true;
				return this;
			}


		}

	}

	this->m_bCurrentlySelected=false;
	return NULL;

}

//************************End of Button Class Funtions********************************
