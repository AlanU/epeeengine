/*
*  EESound.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EESOUND_H
#define EESOUND_H

#include "EpeeEngineCommenIncludes.h"
#include <string>
//***********************Begin Of Sound Class***************************
class Sound
{

	std::string m_sFileName;
	std::string m_sName;
	Mix_Chunk * m_pSound;
	int m_iChannel;
	int m_iNumberOfTimesToLoop;
	int m_iVolume;
public:
	Sound(const std::string & _Name ,const std::string & _FileName,int _Channel=0,int _NumberOfTimesToLoop=0, int _Volume=MIX_MAX_VOLUME);
	~Sound();
	int GetVolume();
	bool SetVolume(int _Volume);
	bool LoadSound();
	Mix_Chunk * GetMixChunkPointer();
	bool SetChannel(int _ChannelToSetTo);
	int GetChannel();
	bool SetNumberOfTimesToLoop(int _NumberOfTimesToLoop);
	int GetNumberOfTimesToLoop();
	std::string GetName();

};
//***********************End Of Sound Class***************************


#endif // EESOUND_H
