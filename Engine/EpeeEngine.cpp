/*
*  EpeeEngine.cpp
*
*
*  EpeeEngine
*	Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/
#pragma warning(disable : 4996)
#define Version_Number_cpp .40
//#include <iostream>

#include <cstdio>
#include <vector>
#include "tinyxml.h"
#include "tinystr.h"
#include "EpeeEngineCommenIncludes.h"
#include "EpeeEngine.h"
#include "EpeeUtility.h"
#include "EELog.h"

#define Graphics_Engine_Credits  " Made with the Epee Engine"
#define DEBUG_ME

#include "EELog.h"









//*************Begin Of ControlCharacters Class Funtions*************************
ControlCharacters::ControlCharacters()
{
	m_bShift=false;
	m_bControl=false;
	m_bAlt=false;
	m_bCommand=false;
	m_bCapsLock=false;
	m_keyCaught=false;
}


void ControlCharacters::ProcessKeyEvent(SDL_Event *_event)
{
	if(_event)
	{
		switch(_event->type)
		{
		case SDL_KEYUP:
			m_keyCaught=true;
			switch(_event->key.keysym.sym)
			{
			case SDLK_RSHIFT:
			case SDLK_LSHIFT:
				m_bShift=false;
				break;
			case SDLK_RCTRL:
			case SDLK_LCTRL:
				m_bControl=false;
				break;
			case SDLK_RALT:
			case SDLK_LALT:
				m_bAlt=false;
				break;

			case SDLK_CAPSLOCK:
				if(m_bCapsLock)
					m_bCapsLock=false;
				else
					m_bCapsLock=true;
				EE_DEBUG<<"Caps lock is "<<m_bCapsLock<<std::endl;
				break;

			default:
				m_keyCaught=false;
				break;
			}


			break;
		case SDL_KEYDOWN:
			m_keyCaught=true;
			switch(_event->key.keysym.sym)
			{
			case SDLK_RSHIFT:
			case SDLK_LSHIFT:
				m_bShift=true;
				break ;
			case SDLK_RCTRL:
			case SDLK_LCTRL:
				m_bControl=true;
				break;
			case SDLK_RALT:
			case SDLK_LALT:
				m_bAlt=true;
				break;
			default:
				m_keyCaught=false;
				break;

			}
			break;
		}
	}
	else
	{
		//EpeeEngineError temperror("Can not Proccess an NULL event pointer",NULL_POINTER_PASSED,this);
		//	 this->SetLastError(temperror);
	}

}


void ControlCharacters::SetCapLock(bool _caps)
{
	EE_DEBUG<<"Caps Lock is was "<<m_bCapsLock<<std::endl;
	m_bCapsLock= _caps;
	EE_DEBUG<<"Caps Lock is now  "<<m_bCapsLock<<std::endl;
}


bool ControlCharacters::GetKeyCaught()
{
	return m_keyCaught;
}
bool ControlCharacters::IsAltPressed()
{
	return m_bAlt;
}

bool ControlCharacters::IsCtrlPressed()
{
	return m_bControl;
}

bool ControlCharacters::IsShiftPressed()
{
	return m_bShift;
}
bool ControlCharacters::IsCapLockPressed()
{
	return m_bCapsLock;
}







//**************************Begin of FileLookUp Class Funtions*****************

FileLookUp::FileLookUp()
{
	m_ssymbol=" ";
	m_sfilename=" ";
}

FileLookUp::FileLookUp(const std::string & _Symbol, const std::string & _fileName)
{

	m_ssymbol=_Symbol;
	m_sfilename=_fileName;

}
//**************************End of FileLookUp Class Funtions*****************



AnimationScript * EpeeEngine::CreateAnimationScript(const std::string &  _Name,EEWidget * _AnimationSprocket,PlayScrtipFuntionPointer* _ScrtipFuntionPointer,int _z,int  _TotalNumberOfFrames,int _fps,int _StartFrame,int _Loop,bool _StartNow,const std::string &   _AddtoCurrentRenderlist)
{
	AnimationScript * temp=new AnimationScript(_Name, _AnimationSprocket,_z,_ScrtipFuntionPointer, _fps, _TotalNumberOfFrames, _StartFrame, _Loop, _StartNow);
	if (temp!=NULL)
	{
		//temp->LoadFileSurface();

		InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));
		return temp;
	}
	else
	{
		return NULL;
	}
	return NULL;
}

double EpeeEngine::GetVersionNumberCpp()
{
	return Version_Number_cpp;
}
double EpeeEngine::GetVersionNumberH()
{
	return Version_Number_h;
}

bool EpeeEngine::ValideVersionNumberOfCppWithHeader()
{
	if (GetVersionNumberCpp()==GetVersionNumberH()) {
		return true;
	}
	else
	{
		EE_ERROR<<"****WARNING**** The version number of the Epee Engine header and cpp file do not match. This may cause Problems"<<std::endl;
		EE_ERROR<<"Epee Engine header file verion number "<<GetVersionNumberH()<<std::endl;
		EE_ERROR<<"Epee Engine Cpp file verion number "<<GetVersionNumberCpp()<<std::endl;
		return false;
	}
}
EpeeEngine::EpeeEngine()
{
	if (ValideVersionNumberOfCppWithHeader()) {
		EE_INFO<<"Epee Engine verion number "<<GetVersionNumberH()<<std::endl;
	}
	else
	{
		EE_WARNING<<"****WARNING**** Epee Engine verion number could not be determined see error log stderr.txt"<<std::endl;
	}
	m_pscreen=NULL;
	FrameRate=NULL;
	//m_pfont1=NULL;
	m_iScreen_Heigth=800;
	m_iScreen_Width=600;
	//m_iWhichScreenDrawing=1;
	m_bFullScreen=false;
	m_bResizeAble=false;
	m_ifps=60;
	m_pSoundOne=NULL;
	m_pBackGroundMusic=NULL;
	m_ifpstime=0;
	m_iframe=0;
	m_pCurrentEditedTextBox=NULL;
	m_fCurrentFrameRate=0;
	m_pDownedButton=NULL;
	//m_pLastSprocketFound=NULL;
	m_u32VideoFlags=SDL_OPENGL;//SDL_SWSURFACE; //|SDL_ANYFORMAT;
	m_uiTotalRenderListsCreated=0;
	m_uiTotalSoundsCreated=0;
	m_uiTotalConnectionsCreated=0;
	m_LastRenderListFound=NULL;
	m_uiCurrentRenderList=NULL;
	m_bHardWareAccelration=1;
	m_cLastError.m_iErrorCode=EPEE_NO_ERROR;
	m_cLastError.m_sError_Message="No Error";
	m_cLastError.ObjectWhereErrorOccured=NULL;
	m_pLastSprocketClicked=NULL;

}


EpeeEngine::EpeeEngine(std::vector<FileLookUp> filetable)
{
	m_cfiletable = filetable;

	if (ValideVersionNumberOfCppWithHeader()) {
		EE_INFO<<"Epee Engine verion number "<<GetVersionNumberH()<<std::endl;
	}
	else
	{
		EE_WARNING<<"****WARNING**** Epee Engine verion number could not be determined see error log stderr.txt"<<std::endl;
	}
	m_pscreen=NULL;
	FrameRate=NULL;
	//m_pfont1=NULL;
	m_iScreen_Heigth=800;
	m_iScreen_Width=600;
	//m_iWhichScreenDrawing=1;
	m_bFullScreen=false;
	m_bResizeAble=false;
	m_ifps=60;
	m_pSoundOne=NULL;
	m_pBackGroundMusic=NULL;
	m_ifpstime=0;
	m_iframe=0;
	m_pDownedButton=NULL;
	m_pCurrentEditedTextBox=NULL;
	m_fCurrentFrameRate=0;
	//m_pLastSprocketFound=NULL;
	m_u32VideoFlags=SDL_OPENGL;//SDL_SWSURFACE ;//|SDL_ANYFORMAT;
	m_uiTotalRenderListsCreated=0;
	m_uiTotalSoundsCreated=0;
	m_uiTotalConnectionsCreated=0;
	m_LastRenderListFound=NULL;
	m_uiCurrentRenderList=NULL;
	m_bHardWareAccelration=1;
	m_cLastError.m_iErrorCode=EPEE_NO_ERROR;
	m_cLastError.m_sError_Message="No Error";
	m_cLastError.ObjectWhereErrorOccured=NULL;
	m_pLastSprocketClicked=NULL;
}



EpeeEngine::~EpeeEngine()
{
	unsigned int TotalRenderListDeleted=0;
	unsigned int  TotalSoundsToDeleted=0;
	//  unsigned int  TotalConnectionsDeleted=0;
	RenderList* temp=NULL;
	if (m_pBackGroundMusic)
	{
		Mix_FreeMusic( m_pBackGroundMusic );
	}

	/*if (m_pfont1)
	{
	TTF_CloseFont(m_pfont1);
	m_pfont1=NULL;
	}*/
	while(m_vRenderList.size()!=0)
	{
		temp=m_vRenderList[0];
		m_vRenderList.erase(m_vRenderList.begin());
		delete temp;
		//DestroySprocket(m_vEEWidgetList[0]->GetName());
		TotalRenderListDeleted++;
	}

	while(m_vSounds.size()!=0)
	{
		DestroySound(m_vSounds[0]->GetName());
		TotalSoundsToDeleted++;
	}



	//	m_vTextBoxList.clear();
	if (TTF_WasInit())
		TTF_Quit();
	if(SDL_WasInit(SDL_INIT_AUDIO))
	{
		Mix_CloseAudio();

	}

	SDL_Quit();


	if (m_uiTotalRenderListsCreated-TotalRenderListDeleted!=0) {
		EE_ERROR<<"*****WARING MEMORY LEAK****"<<std::endl;
		EE_ERROR<<"There are still "<<m_uiTotalRenderListsCreated<<" Render List that where not deleted"<<std::endl;
	}

	if (m_uiTotalSoundsCreated) {
		EE_ERROR<<"*****WARING MEMORY LEAK****"<<std::endl;
		EE_ERROR<<"There are still "<<m_uiTotalSoundsCreated<<" Sounds that where not deleted"<<std::endl;
	}
	if (m_uiTotalConnectionsCreated) {
		EE_ERROR<<"*****WARING MEMORY LEAK****"<<std::endl;
		EE_ERROR<<"There are still "<<m_uiTotalConnectionsCreated<<" Connections that where not deleted"<<std::endl;
	}

	EELog::Destroy();
}

bool EpeeEngine::SetUp(unsigned int Screen_Width,unsigned int Screen_Heigth,bool FullSreenMode,std::string  _WindowName,bool _Resize,unsigned int _fps,unsigned int _BitDepthOfScreen,bool Anyformat)
{
	_Resize=false;




	if (!TTF_WasInit())
	{
		if(TTF_Init()==-1)
		{
			EE_ERROR<<"TTR_Init:" <<TTF_GetError()<<std::endl;
		}

	}


	m_iScreen_Heigth=Screen_Heigth;
	m_iScreen_Width=Screen_Width;
	// Initialize SDL's subsystems - in this case, only video.
	if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 )
	{
		EE_ERROR<<"Unable to init SDL: "<< SDL_GetError()<<std::endl;
		return false;
	}


	// Register SDL_Quit to be called at exit; makes sure things are
	// cleaned up when we quit.
	//atexit(SDL_Quit);

	// Attempt to create a  Screen_Height X Screen_Width window with 32bit pixels.
	if(FullSreenMode)
	{
		m_u32VideoFlags= m_u32VideoFlags|SDL_FULLSCREEN;
	}
	if(_Resize)
		m_u32VideoFlags=m_u32VideoFlags|SDL_RESIZABLE;
	if( !(_BitDepthOfScreen==8 || _BitDepthOfScreen==16 || _BitDepthOfScreen==24 ||  _BitDepthOfScreen==32 ))
	{
		EE_ERROR<<_BitDepthOfScreen<<" is not a vaild BitDepth for the display. Your screen bit depth is now 16"<<std::endl;
		_BitDepthOfScreen=16;
	}
	//	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
	//SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
	//SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
	//SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute( SDL_GL_SWAP_CONTROL, 0);
	//SDL_GL_SetAttribute( SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute( SDL_GL_BUFFER_SIZE,_BitDepthOfScreen);

	if (Anyformat)
	{
		m_pscreen = SDL_SetVideoMode(Screen_Width,Screen_Heigth,_BitDepthOfScreen, m_u32VideoFlags|SDL_ANYFORMAT);
	}
	else
	{
		m_pscreen = SDL_SetVideoMode(Screen_Width,Screen_Heigth,_BitDepthOfScreen, m_u32VideoFlags);
	}
	//m_pMainScreen2 =SDL_SetVideoMode(Screen_Heigth, Screen_Width, 32, SDL_SWSURFACE);


	// If we fail, return error.
	if ( m_pscreen == NULL )
	{
		m_bFullScreen=FullSreenMode;
		m_bResizeAble=_Resize;
		EE_ERROR<<"Unable to set"<<Screen_Heigth<<"x"<<Screen_Width<<" video: "<<SDL_GetError()<<std::endl;
		return false;
	}

	_WindowName = _WindowName+Graphics_Engine_Credits;
	SDL_WM_SetCaption( _WindowName.c_str(),NULL);
	m_ifps=_fps;

	//int temp67=SDL_getFramerate(& m_cfpsManager);
	// Mix_OpenAudio(44100, AUDIO_S16, 2, 1024) == -1 ;
	if( Mix_OpenAudio(22050, AUDIO_S16, 2, 1024) == -1 ) //Check return type
	{
		return false;
	}
	else
	{
		Mix_VolumeMusic(MIX_MAX_VOLUME);
	}
	CreateRenderList("Default");
	this->CreateTextBox("FrameRate",0,20,50," ","Default",-1,-1);
	SetCurrentRenderList("Default");

	FrameRate=(textBox *)FindEEWidget("FrameRate");
	FrameRate->SetFontPoint(12);
	FrameRate->SetQuality(FONT_QUALITY_HIGH);
	FrameRate->SetText("0");
	m_ifpstime=SDL_GetTicks();
	char videodriver[256] ;


	SDL_VideoDriverName(videodriver,255);
#ifndef NDEBUG
	FrameRate->SetDraw(false);
#endif

	SDL_EnableKeyRepeat(300,30);

	glClearColor(0, 0, 0, 0 );


	glEnable( GL_TEXTURE_2D ); // Need this to display a texture
	glViewport( 0, 0,  m_iScreen_Width,  m_iScreen_Heigth );

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	glOrtho( 0, m_iScreen_Width, m_iScreen_Heigth, 0, -1, 1 );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();



	return true;
}


bool EpeeEngine::SetUp(std::vector<FileLookUp> filetable,unsigned int Screen_Width,unsigned int Screen_Heigth,bool FullSreenMode,const std::string &  _WindowName,bool _Resize,unsigned int _fps,unsigned int _BitDepthOfScreen,bool Anyformat)
{
	if(SetUp(Screen_Width,Screen_Heigth,FullSreenMode,_WindowName,_Resize,_fps,_BitDepthOfScreen,Anyformat))
	{
		m_cfiletable =filetable;
		return true;
	}
	else
		return false;


}

bool EpeeEngine::SetAndPlayBackGroundMusic(const std::string &  _NameOfAudio,int _NumberOfTimesToLoop,int _Volume,int _ms)
{
	std::string error;

	if (m_pBackGroundMusic)
	{
		Mix_FreeMusic( m_pBackGroundMusic );
	}


	m_pBackGroundMusic = Mix_LoadMUS( _NameOfAudio.c_str() );



	if (m_pBackGroundMusic)
	{
		if(_ms==0)
		{
			Mix_VolumeMusic( _Volume);

			Mix_PlayMusic( m_pBackGroundMusic, _NumberOfTimesToLoop );
		}
		else
		{
			Mix_FadeInMusic( m_pBackGroundMusic, _NumberOfTimesToLoop ,_ms);
		}

		return true;

	}
	else
	{
		error=Mix_GetError();
		return false;
	}

}


bool EpeeEngine::IsSoundPlaying(Sound *_pSound)
{
	if(_pSound)
	{
		return Mix_Playing(_pSound->GetChannel()) == 0 ? false : true;
	}
	else
	{
		EpeeEngineError temperror("Null Pointer Passed in to PlaySoundNow",NULL_POINTER_PASSED,this);
		this->SetLastError(temperror);

	}
	return false;
}

bool EpeeEngine::IsBackGoundMusicPlaying()
{
	return Mix_PlayingMusic() == 0 ? false : true;
}


bool EpeeEngine::StopBackGroundMusic(int _ms)
{

	Mix_FadeOutMusic(_ms);
	return true;
}



bool EpeeEngine::SetNumberOfMixingChannels(int _NumberOfChannels)
{
	Mix_AllocateChannels(_NumberOfChannels);
	return true;
}
bool EpeeEngine::PlaySoundNow(const std::string &  _NameOfAudio,int _NumberOfTimesToLoop,int _Channel)
{
	std::string error;

	if (m_pSoundOne)

	{
		m_uiTotalSoundsCreated--;
		Mix_FreeChunk(m_pSoundOne);
	}

	m_pSoundOne=Mix_LoadWAV(_NameOfAudio.c_str());
	if(Mix_PlayChannel( _Channel, m_pSoundOne, _NumberOfTimesToLoop) <0)
	{

		EpeeEngineError temperror(Mix_GetError(),SOUND_PLAYING_ERROR,this);
		this->SetLastError(temperror);

		return false;
	}
	else
	{
		m_uiTotalSoundsCreated++;
		return true;
	}


}

bool EpeeEngine::PlaySoundNow(Sound * _pSound)
{
	std::string error;

	if (_pSound)

	{
		Mix_Volume(_pSound->GetChannel(),_pSound->GetVolume());
		if(Mix_PlayChannel(_pSound->GetChannel(), _pSound->GetMixChunkPointer(), _pSound->GetNumberOfTimesToLoop()) <0)
		{
			EpeeEngineError temperror(Mix_GetError(),SOUND_PLAYING_ERROR,this);
			this->SetLastError(temperror);
			error=Mix_GetError();
			return false;
		}
		else
			return true;
	}
	else
	{

		EpeeEngineError temperror("Null Pointer Passed in to PlaySoundNow",NULL_POINTER_PASSED,this);
		this->SetLastError(temperror);

		return false;
	}

}

bool EpeeEngine::FadeInSound(Sound * _pSound,int _ms)
{
	std::string error;
	if(_ms>0)
	{
		if (_pSound)

		{
			Mix_Volume(_pSound->GetChannel(),_pSound->GetVolume());
			if(Mix_FadeInChannel( _pSound->GetChannel(), _pSound->GetMixChunkPointer(), _pSound->GetNumberOfTimesToLoop(),_ms) <0)
			{
				EpeeEngineError temperror(Mix_GetError(),SOUND_PLAYING_ERROR,this);
				this->SetLastError(temperror);
				error=Mix_GetError();
				return false;
			}
			else
				return true;
		}
		else
		{

			EpeeEngineError temperror("Null Pointer Passed in to PlaySoundNow",NULL_POINTER_PASSED,this);
			this->SetLastError(temperror);

			return false;
		}
	}
	EpeeEngineError temperror("Can not fade sound in with a negitve value",SOUND_PLAYING_ERROR,this);
	this->SetLastError(temperror);
	return false;
}


bool EpeeEngine::FadeOutSound(int _Channel,int _ms)
{
	if(_ms>0)
	{
		Mix_FadeOutChannel(_Channel,_ms);
		return true;
	}
	EpeeEngineError temperror("Can not fade sound out with a negitve value",SOUND_PLAYING_ERROR,this);
	this->SetLastError(temperror);
	return false;


}

bool EpeeEngine::FadeOutSound(Sound * _pSound,int _ms)
{

	return FadeOutSound(_pSound->GetChannel(), _ms);
}





bool EpeeEngine::StopSoundNow(int _Channel)
{
	Mix_HaltChannel(_Channel);
	return true;
}

bool EpeeEngine::StopSoundNow(Sound * _pSound)
{
	if(_pSound)
	{
		return StopSoundNow(_pSound->GetChannel());
	}
	else
	{
		EpeeEngineError temperror("Null Pointer Passed in to PlaySoundNow",NULL_POINTER_PASSED,this);
		this->SetLastError(temperror);
	}
	return false;
}







Sound * EpeeEngine::CreateSound(const std::string &  _GroupName,const std::string &  _FileName,int _Channel,int _NumberOfTimesToLoop,int _Volume)
{
	Sound * temp =new Sound(_GroupName ,_FileName,_Channel,_NumberOfTimesToLoop,_Volume);
	if (temp)
	{
		if (!temp->LoadSound())
		{
			EpeeEngineError temperror(Mix_GetError(),SOUND_LOADING_ERROR,this);
			this->SetLastError(temperror);
			delete temp;
			return NULL;
		}
		else
		{

			m_uiTotalSoundsCreated++;
			m_vSounds.push_back(temp);
		}
	}

	else
	{

		EpeeEngineError temperror("Could not Create sound "+_FileName,SOUND_LOADING_ERROR,this);
		this->SetLastError(temperror);
	}

	return temp; // need to add error return
}

bool EpeeEngine::DestroySound(const std::string &  _name)
{
	Sound * temp=NULL;
	bool retunval=false;
	for(unsigned int index = 0;index<m_vSounds.size();index++)
	{
		if(m_vSounds[index]->GetName()==_name)
		{
			temp=m_vSounds[index];
			m_vSounds.erase(m_vSounds.begin()+index);
			//Mix_FreeChunk(temp->GetMixChunkPointer());
			delete temp;
			m_uiTotalSoundsCreated--;
			retunval=true;
			index=m_vSounds.size();

		}
	}


	EpeeEngineError temperror("Could not find and delete sound "+_name,SOUND_DELETING_ERROR,this);
	this->SetLastError(temperror);;
	return retunval;
}


Sound *  EpeeEngine::FindSound(const std::string &  _name)
{
#if defined(DEBUG_ME)
	std::string temp_name=" ";
#endif

	for (unsigned int index=0;index<m_vSounds.size();index++)
	{
#if defined(DEBUG_ME)
		temp_name=m_vSounds[index]->GetName();
		if(temp_name==_name)
		{
			return m_vSounds[index];

		}
#else
		if(m_vSounds[index]->GetName()==_name)
		{
			return m_vSounds[index];

		}

#endif


	}

	EpeeEngineError temperror("Could not find sound "+_name,SOUND_NOT_FOUND,this);
	this->SetLastError(temperror);

	return NULL;
}

ButtonTB * EpeeEngine::CreateButtonTB(const std::string &  _ButtonName,int _Buttonx,int _Buttony,int _Buttonz,const std::string &  _ButtonText ,bool _SButtonactive,const std::string &   _AddtoCurrentRenderlist,unsigned int _ButtonTB_height,unsigned int _ButtonTB_width,const std::string &  _ButtonTB_Font,int _ButtonTB_FontPoint,unsigned int _ButtonTB_red,unsigned int _ButtonTB_blue ,unsigned int _ButtonTB_green,const std::string &  _ButtonFontPath)
{ButtonTB *temp=new ButtonTB( _ButtonName, _Buttonx, _Buttony, _Buttonz, _ButtonText , _SButtonactive, _ButtonTB_height,_ButtonTB_width, _ButtonTB_Font,_ButtonTB_FontPoint, _ButtonTB_red, _ButtonTB_blue , _ButtonTB_green, _ButtonFontPath);
if (temp!=NULL) {


	temp->LoadFont();

	InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));

	//m_uiTotalEEWidgetCreated++;
	return temp;
}
else
{
	return NULL;
}
}

textBox * EpeeEngine::CreateTextBox(const std::string &  _Name,int _x,int _y,int _z,const std::string &  _TextBoxMessage,const std::string &   _AddtoCurrentRenderlist,unsigned int _height,unsigned int _width,const std::string &  _Font,int _FontPoint,unsigned int _red,unsigned int _blue ,unsigned int _green,const std::string &  _FontPath)
{textBox *temp=new textBox( _Name, _x, _y,_z,_TextBoxMessage, _height, _width,_Font, _FontPoint, _red, _blue ,_green,_FontPath);

if (temp!=NULL) {

	temp->LoadFont();

	InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));

	//m_uiTotalEEWidgetCreated++;
	return temp;
}
else
{
	return NULL;
}
}


image * EpeeEngine::CreateImage(const std::string &  _fileName ,const std::string &  _Name,int _x,int _y,int _z,const std::string &   _AddtoCurrentRenderlist,bool _Transparency)
{image *temp=new image( _fileName ,_Name, _x, _y,_z,_Transparency);
if (temp!=NULL) {

	//temp->LoadFileSurface();

	InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));

	//m_uiTotalEEWidgetCreated++;
	return temp;
}
else
{
	return NULL;
}

}

Animation * EpeeEngine::CreateAnimation(const std::string &  _fileName ,const std::string &  _Name,int _x,int _y,int _z,int _NumberOfFrames,int _fps,int _StartFrame,int _Loop,int _NumberOfRowsOfFrames,bool _Transparency,bool _StartNow,const std::string &   _AddtoCurrentRenderlist)
{Animation * temp=new Animation( _fileName, _Name,_fps, _x, _y, _z, _NumberOfFrames,_StartFrame,_Loop,_Transparency, _StartNow,_NumberOfRowsOfFrames);
if (temp!=NULL) {

	//temp->LoadFileSurface();
	temp->Init();
	InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));

	// m_uiTotalEEWidgetCreated++;
	return temp;
}
else
{
	return NULL;
}

}
bool EpeeEngine::ChangeSprocketZ(EEWidget * _SprocketToChange,int _newZ,RenderList * _list)
{
	if (_list==NULL) {

		if (m_uiCurrentRenderList) {
			_list=m_uiCurrentRenderList;
		}
		else
		{
			return false;
		}
	}

	_list->UpdateVectorlocation();
	_list->m_vEEWidgetList.erase(_list->m_vEEWidgetList.begin()+_SprocketToChange-> GetCurrentVectorPostion());
	_SprocketToChange->SetLocationZ(_newZ);
	_list->InsertSprocket(_SprocketToChange,false);
	return true;

}

bool EpeeEngine::InsertRenderList(RenderList * _newRenderList)
{

	m_vRenderList.push_back(_newRenderList);

	return true;

}


bool EpeeEngine::DestroyRenderList(const std::string &  _name)
{
	RenderList * temp=NULL;
	bool retunval=false;
	if (_name=="Default") {
		return false;
	}
	for(unsigned int index = 0;index<m_vRenderList.size();index++)
	{
		if(m_vRenderList[index]->GetName()==_name)
		{
			if (_name==m_uiCurrentRenderList->GetName() ) {
				m_uiCurrentRenderList=FindRenderList("Default");
			}
			if(m_LastRenderListFound)
			{
				if(m_LastRenderListFound->GetName()==_name)
					m_LastRenderListFound=NULL;
			}
			temp=m_vRenderList[index];
			m_vRenderList.erase(m_vRenderList.begin()+index);
			delete temp;
			m_uiTotalRenderListsCreated--;
			retunval=true;
			index=m_vRenderList.size();

		}
	}
	if (!retunval) {


		EpeeEngineError temperror("Could Not Find And Delete RenderList "+_name,RENDERLIST_DELETING_ERROR,this);
		this->SetLastError(temperror);
		EE_ERROR<<"Could Not Find And Delete RenderList "<<_name<<std::endl;
	}


	return retunval;
}


bool EpeeEngine::RemoveIteamFromRendableList(const std::string &  _card,const std::string &  _renderList)
{
	RenderList * temp=FindRenderList(_renderList);

	if (temp) {
		return temp->DestroySprocket(_card);
	}
	return false;
}



RenderList*  EpeeEngine::FindRenderList(const std::string &  _name)
{
#if defined(DEBUG_ME)
	std::string temp_name=" ";
#endif
	if (_name=="Current" || _name=="current") {
		return GetCurrentRenderListptr();
	}
	if(m_LastRenderListFound)
	{

		if(m_LastRenderListFound->GetName()==_name)
		{
			return m_LastRenderListFound;
		}
	}
	for (unsigned int index=0;index<m_vRenderList.size();index++)
	{
#if defined(DEBUG_ME)
		temp_name=m_vRenderList[index]->GetName();
		if(temp_name==_name)
		{
			m_LastRenderListFound=m_vRenderList[index];
			return m_vRenderList[index];

		}
#else





		if(m_vRenderList[index]->GetName()==_name)
		{
			m_LastRenderListFound=m_vRenderList[index];
			return m_vRenderList[index];

		}

#endif


	}
	EpeeEngineError temperror("Could Not Find Renderlist "+_name,RENDERLIST_NOT_FOUND,this);
	this->SetLastError(temperror);

	return NULL;

}



ButtonTB * EpeeEngine::FindButtonTB(const std::string &  _name)
{
	ButtonTB *temp=(ButtonTB*)FindEEWidget(_name);
	if(temp)
	{

		if (temp->GetType()!=Type_ButtonTB) {
			EpeeEngineError temperror(_name+" is not an ButtonTB object",SPROCKET_ERROR,this);
			this->SetLastError(temperror);
			return NULL;
		}
	}

	else
		return NULL;



	return temp;
}

textBox * EpeeEngine::FindTextBox(const std::string &  _name)
{
	textBox *temp=(textBox*)FindEEWidget(_name);

	if(temp)
	{
		if (temp->GetType()!=Type_TextBox) {
			EpeeEngineError temperror(_name+" is not an textBox object",SPROCKET_ERROR,this);
			this->SetLastError(temperror);

			return NULL;
		}
	}

	else
		return NULL;


	return (textBox*)FindEEWidget(_name);
}

Button * EpeeEngine::FindButton(const std::string &  _name)
{
	Button *temp=(Button*)FindEEWidget(_name);
	if(temp)
	{
		if (temp->GetType()!=Type_Button) {
			EpeeEngineError temperror(_name+" is not an Button object",SPROCKET_ERROR,this);
			this->SetLastError(temperror);
			return NULL;
		}
	}
	else
		return NULL;

	return (Button*)FindEEWidget(_name) ;
}

image *EpeeEngine::FindImage(const std::string &  _name)
{
	image *temp=(image*)FindEEWidget(_name);
	if(temp)
	{
		if (temp->GetType()!=Type_Image) {

			EpeeEngineError temperror(_name+" is not an image object",SPROCKET_ERROR,this);
			this->SetLastError(temperror);
			return NULL;
		}
	}
	else
		return NULL;


	return (image*)FindEEWidget(_name) ;
}

Animation *EpeeEngine::FindAnimation(const std::string &  _name)
{
	Animation *temp=(Animation*)FindEEWidget(_name);
	if(temp)
	{
		if (temp->GetType()!=Type_Animation) {

			EpeeEngineError temperror(_name+" is not an Animation object",SPROCKET_ERROR,this);
			this->SetLastError(temperror);

			return NULL;
		}
	}
	else
		return NULL;


	return (Animation*)FindEEWidget(_name) ;
}


image * EpeeEngine::CreateImageFromList(const std::string &  _symbol,const std::string &  _ImageName,int _x,int _y,int _z)
{
	image * temp=NULL;
	for(unsigned int i=0;i<m_cfiletable.size();i++)
	{

		if(m_cfiletable[i].m_ssymbol==_symbol)
		{

			temp=this->CreateImage(m_cfiletable[i].m_sfilename.c_str(),_ImageName,_x,_y,_z);
			break;

		}//end of  if(m_cfiletable[i].m_scardsymbol==card)

	}//end of for loop


	return temp;
}

void EpeeEngine::DrawOutlineWithRect(SDL_Surface * _SurfaceToBeOutLined,SDL_Rect *ClipOutLine,Uint8 _red,Uint8 _blue,Uint8 _green,unsigned int _thickness )
{
	Uint32 fillcolor=SDL_MapRGB(_SurfaceToBeOutLined->format,_red, _blue,_green);
	//Uint32 fillcolor=SDL_MapRGBA(_SurfaceToBeOutLined->format,_red, _blue,_green,255);
	SDL_Rect OutLine;

	OutLine.x=ClipOutLine->x;
	OutLine.y=ClipOutLine->y;
	OutLine.h=_thickness ;
	OutLine.w=ClipOutLine->w;
	SDL_FillRect(_SurfaceToBeOutLined,&OutLine,fillcolor);

	OutLine.x=(ClipOutLine->x +ClipOutLine->w)-1;
	OutLine.y=ClipOutLine->y;
	OutLine.h=ClipOutLine->h;
	OutLine.w=_thickness ;
	SDL_FillRect(_SurfaceToBeOutLined,&OutLine,fillcolor);

	OutLine.x=ClipOutLine->x;
	OutLine.y=(ClipOutLine->y+ClipOutLine->h)-1;
	OutLine.h=_thickness ;
	OutLine.w=ClipOutLine->w;
	SDL_FillRect(_SurfaceToBeOutLined,&OutLine,fillcolor);

	OutLine.x=ClipOutLine->x;
	OutLine.y=ClipOutLine->y;
	OutLine.h=ClipOutLine->h;
	OutLine.w=_thickness ;
	SDL_FillRect(_SurfaceToBeOutLined,&OutLine,fillcolor);
}
void EpeeEngine::DrawOutline(SDL_Surface * _SurfaceToBeOutLined,unsigned int _x,unsigned int _y,Uint8 _red,Uint8 _blue,Uint8 _green,unsigned int _thickness )
{
	SDL_Rect OutLine;
	OutLine.x=0;
	OutLine.y=0;
	OutLine.h=_SurfaceToBeOutLined->h;
	OutLine.w=_SurfaceToBeOutLined->w;
	DrawOutlineWithRect(_SurfaceToBeOutLined,&OutLine,_red,_blue,_green,_thickness);

}
/*
bool EpeeEngine::DrawImage(image* _ImageToDraw)
{
SDL_Surface *image_surface=NULL;
SDL_Surface * TempSurface=NULL;
//SDL_Surface *TempSurface2=NULL;
bool KeyColor=true;

Uint32 flags=0;
SDL_Rect ClippingRect;

SDL_Rect * OrignalRectPonter=_ImageToDraw->GetRect();
SDL_Rect OrignalRect;
OrignalRect.x=OrignalRectPonter->x;
OrignalRect.y=OrignalRectPonter->y;
OrignalRect.h=OrignalRectPonter->h;
OrignalRect.w=OrignalRectPonter->w;

if(_ImageToDraw->GetBlit())
{
_ImageToDraw->LoadFileSurface();
}


if(_ImageToDraw->GetEEWidgeturface())
{


if((_ImageToDraw->GetRotation()!=0 || _ImageToDraw->GetScalingFactorX()!=1 ||  _ImageToDraw->GetScalingFactorY()!=1 )&& _ImageToDraw->GetModifySurFace()==NULL )
{
if(_ImageToDraw->GetClip())
{	ClippingRect.x=_ImageToDraw->GetClippingX();
ClippingRect.y=_ImageToDraw->GetClippingY();
ClippingRect.h=_ImageToDraw->GetClippingH();
ClippingRect.w=_ImageToDraw->GetClippingW();
TempSurface=SDL_CreateRGBSurface(_ImageToDraw->GetEEWidgeturface()->flags, ClippingRect.w,ClippingRect.h,_ImageToDraw->GetEEWidgeturface()->format->BitsPerPixel,_ImageToDraw->GetEEWidgeturface()->format->Rmask,_ImageToDraw->GetEEWidgeturface()->format->Gmask,_ImageToDraw->GetEEWidgeturface()->format->Bmask,_ImageToDraw->GetEEWidgeturface()->format->Amask);

if (_ImageToDraw->GetTransparency())
{

_ImageToDraw->GetEEWidgeturface()->flags=flags;
_ImageToDraw->GetEEWidgeturface()->flags=SDL_SWSURFACE;
}
if (TempSurface)
{
SDL_BlitSurface(_ImageToDraw->GetEEWidgeturface(),&ClippingRect,TempSurface,NULL);
flags=_ImageToDraw->GetEEWidgeturface()->flags;
KeyColor=false;
if (_ImageToDraw->GetTransparency())
{
TempSurface->flags=TempSurface->flags|SDL_SRCCOLORKEY;
TempSurface->format->colorkey=_ImageToDraw->GetEEWidgeturface()->format->colorkey;
}
}
else
{
return false;
}
}
if(!TempSurface)
{
image_surface= rotozoomSurfaceXY(_ImageToDraw->GetEEWidgeturface(), _ImageToDraw->GetRotation(),_ImageToDraw->GetScalingFactorX(),_ImageToDraw->GetScalingFactorY() ,_ImageToDraw->GetAntiAliased());

}
else
{
image_surface= rotozoomSurfaceXY(TempSurface, _ImageToDraw->GetRotation(),_ImageToDraw->GetScalingFactorX(),_ImageToDraw->GetScalingFactorY() ,_ImageToDraw->GetAntiAliased());
}
}
else
{

_ImageToDraw->SetHeightWidth(_ImageToDraw->GetEEWidgeturface()->h,_ImageToDraw->GetEEWidgeturface()->w);
if (_ImageToDraw->GetTransparency() && KeyColor)
{
if(!SetKeyColor(_ImageToDraw->GetEEWidgeturface()))
{
EpeeEngineError temperror(_ImageToDraw->GetName()+" Could not set the color key",RENDERING_ERROR,this);
this->SetLastError(temperror);

}
}
if (_ImageToDraw->GetClip())
{
//This is do to save calls
ClippingRect.x=_ImageToDraw->GetClippingX();
ClippingRect.y=_ImageToDraw->GetClippingY();
ClippingRect.h=_ImageToDraw->GetClippingH();
ClippingRect.w=_ImageToDraw->GetClippingW();

if(_ImageToDraw->GetModifySurFace()!=NULL)
{

if(_ImageToDraw->GetOutLine())
{
DrawOutlineWithRect(_ImageToDraw->GetModifySurFace(),&ClippingRect);
}
SDL_BlitSurface(_ImageToDraw->GetModifySurFace(),&ClippingRect,m_pscreen,&OrignalRect);
_ImageToDraw->SetHeightWidth(_ImageToDraw->GetModifySurFace()->h,_ImageToDraw->GetModifySurFace()->w);

}
else
{
if(_ImageToDraw->GetOutLine())
{
DrawOutlineWithRect(_ImageToDraw->GetEEWidgeturface(),&ClippingRect);
}
SDL_BlitSurface(_ImageToDraw->GetEEWidgeturface(),&ClippingRect,m_pscreen,&OrignalRect);
_ImageToDraw->SetHeightWidth(_ImageToDraw->GetEEWidgeturface()->h,_ImageToDraw->GetEEWidgeturface()->w);

}
}
else
{
if(_ImageToDraw->GetModifySurFace()!=NULL)
{
if(_ImageToDraw->GetOutLine())
{
DrawOutline(_ImageToDraw->GetModifySurFace(),_ImageToDraw->GetLocationX(),_ImageToDraw->GetLocationY());
}
SDL_BlitSurface(_ImageToDraw->GetModifySurFace(),NULL,m_pscreen,&OrignalRect);
_ImageToDraw->SetHeightWidth(_ImageToDraw->GetModifySurFace()->h,_ImageToDraw->GetModifySurFace()->w);

}
else
{
if(_ImageToDraw->GetOutLine())
{
DrawOutline(_ImageToDraw->GetEEWidgeturface(),_ImageToDraw->GetLocationX(),_ImageToDraw->GetLocationY());
}
SDL_BlitSurface(_ImageToDraw->GetEEWidgeturface(),NULL,m_pscreen,&OrignalRect);
_ImageToDraw->SetHeightWidth(_ImageToDraw->GetEEWidgeturface()->h,_ImageToDraw->GetEEWidgeturface()->w);
}



}
return true;

}


if(TempSurface)
{
SDL_FreeSurface(TempSurface);

}



if(image_surface)
{


_ImageToDraw->SetHeightWidth(image_surface->h,image_surface->w);
if (_ImageToDraw->GetTransparency() && KeyColor) {
SetKeyColor(image_surface);
}
if(_ImageToDraw->GetOutLine())
{
DrawOutline(image_surface,0,0);
}
SDL_BlitSurface(image_surface,NULL,m_pscreen,&OrignalRect);
_ImageToDraw->SetHeightWidth(image_surface->h,image_surface->w);


//SDL_UpdateRects(m_pscreen, 1, &(_ImageToDraw->GetRect()));

//SDL_FreeSurface(image_surface);
_ImageToDraw->SetModifySurFace(image_surface);

return true;
}
else
{
SDL_FreeSurface(image_surface);

return false;
}
return false;
}
return true;
}

*/









/*
bool EpeeEngine::WriteText(textBox* _TextBox)
{
//std::string temp="C:\\WINDOWS\\Fonts\\";
bool returnVal=false;
bool tanslationflag=false;
//temp=temp+_TextBox->GetFont();
SDL_Rect temprect;
temprect.h=_TextBox->GetRect()->h;
temprect.w=_TextBox->GetRect()->w;
temprect.x=_TextBox->GetRect()->x;
temprect.y=_TextBox->GetRect()->y;
temprect.x=temprect.x+_TextBox->GetJustifyOffset();
SDL_Rect temprect12;
temprect12.h=_TextBox->GetHeight();
temprect12.w=_TextBox->GetWidth();
temprect12.x=0;
temprect12.y=0;*/


/*	SDL_Color color;
color.r = 255;
color.g = 255;
color.b = 255;*/
//  SDL_Surface *text_surface_original=NULL;

/*	SDL_Surface *text_surface2=NULL;
SDL_Surface *text_surface3=NULL;
SDL_Palette* text_surface3palette=NULL;
if(_TextBox->GetOutLine() && _TextBox->GetScalingFactorX()==1 &&_TextBox->GetScalingFactorX()==1 )
{
if(_TextBox->GetQuality()!=FONT_QUALITY_SHADED )
{
//_TextBox->m_porignalSurface=SDL_DisplayFormat(_TextBox->m_porignalSurface);
}
else
{
//	_TextBox->m_porignalSurface=SDL_DisplayFormatAlpha(_TextBox->m_porignalSurface);
}
}
if(_TextBox->GetFontPointer())
{
if(_TextBox->GetBlit()==true )
{
_TextBox->SetSurface();

//text_surface2=TTF_RenderText_Solid(m_pfont1,_TextBox->GetText().c_str(),_TextBox->GetTextColor());
}
else
{
if(!_TextBox->GetEEWidgeturface())
{
_TextBox->SetSurface();
}

}
//temprect12.h=_TextBox->GetEEWidgeturface()->h;
//temprect12.w=_TextBox->GetEEWidgeturface()->w;
if((_TextBox->GetRotation()!=0 || _TextBox->GetScalingFactorX()!=1 ||  _TextBox->GetScalingFactorY()!=1 )&& _TextBox->GetModifySurFace()==NULL)
{

if(_TextBox->GetQuality()!=FONT_QUALITY_SHADED && !(_TextBox->WasLastSprocketClicked() && _TextBox->GetEditable() && _TextBox->IsColorChangedOnClick()))
{
text_surface3=SDL_CreateRGBSurface(SDL_SWSURFACE, temprect12.w, temprect12.h, 8, 0, 0, 0, 0);
text_surface3palette = text_surface3->format->palette;
text_surface3palette->colors[0].r = 255 - _TextBox->GetTextColor().r;
text_surface3palette->colors[0].g = 255 - _TextBox->GetTextColor().g;
text_surface3palette->colors[0].b = 255 - _TextBox->GetTextColor().b;
text_surface3palette->colors[1].r = _TextBox->GetTextColor().r;
text_surface3palette->colors[1].g = _TextBox->GetTextColor().g;
text_surface3palette->colors[1].b = _TextBox->GetTextColor().b;
SDL_SetColorKey( text_surface3, SDL_SRCCOLORKEY, 0 );
}
else
{
text_surface3=SDL_CreateRGBSurface(SDL_SWSURFACE, temprect12.w, temprect12.h, 8,_TextBox->GetBackGroundColor().r,_TextBox->GetBackGroundColor().g,_TextBox->GetBackGroundColor().b,0);

text_surface3palette = text_surface3->format->palette;
int reddiffrent = _TextBox->GetTextColor().r - _TextBox->GetBackGroundColor().r;
int greendiffrent = _TextBox->GetTextColor().g - _TextBox->GetBackGroundColor().g;
int bluediffrent = _TextBox->GetTextColor().b - _TextBox->GetBackGroundColor().b;
for( int i = 0; i < 256; ++i )
{
text_surface3palette->colors[i].r = _TextBox->GetBackGroundColor().r + (i*reddiffrent) / (256-1);
text_surface3palette->colors[i].g = _TextBox->GetBackGroundColor().g + (i*greendiffrent) / (256-1);
text_surface3palette->colors[i].b = _TextBox->GetBackGroundColor().b + (i*bluediffrent) / (256-1);
}
text_surface3=SDL_DisplayFormat(text_surface3);
}
temprect12.h=_TextBox->GetHeight();
temprect12.w=_TextBox->GetWidth();
int u=SDL_BlitSurface(_TextBox->GetEEWidgeturface(),&temprect12,text_surface3,NULL);
if(_TextBox->GetQuality()==FONT_QUALITY_HIGH)
{
text_surface3=SDL_DisplayFormatAlpha(text_surface3);
}
if (m_bFullScreen)
{
text_surface2= rotozoomSurfaceXY(text_surface3, _TextBox->GetRotation(),_TextBox->GetScalingFactorX(),_TextBox->GetScalingFactorY() ,0);
tanslationflag=true;
}
else
{
text_surface2= rotozoomSurfaceXY(text_surface3, _TextBox->GetRotation(),_TextBox->GetScalingFactorX(),_TextBox->GetScalingFactorY() ,_TextBox->GetAntiAliased());
tanslationflag=true;
}


}


if(text_surface2)
{
if(_TextBox->GetOutLine())
{
DrawOutline(text_surface2,_TextBox->GetLocationX(),_TextBox->GetLocationY());
}
SDL_BlitSurface(text_surface2,NULL,m_pscreen,&temprect);

//SDL_UpdateRects(m_pscreen, 1, &(_TextBox->GetRect()));
if(text_surface3){
SDL_FreeSurface(text_surface3);}
_TextBox->SetModifySurFace(text_surface2);
//_TextBox->SetHeightWidth(_TextBox->GetModifySurFace()->h,_TextBox->GetModifySurFace()->w);

//SDL_FreeSurface(text_surface2);
return true;
}
else if ( _TextBox->GetEEWidgeturface())
{

if( _TextBox->GetModifySurFace()!=NULL)
{
if(_TextBox->GetOutLine())
{
DrawOutline(_TextBox->GetModifySurFace(),_TextBox->GetLocationX(),_TextBox->GetLocationY());
}

SDL_BlitSurface( _TextBox->GetModifySurFace(),NULL,m_pscreen,&temprect);
//	_TextBox->SetHeightWidth(_TextBox->GetModifySurFace()->h,_TextBox->GetModifySurFace()->w);

}
else
{

SDL_Rect outlinerect;

temprect12.h=_TextBox->GetHeight();
temprect12.w=_TextBox->GetWidth();
outlinerect.x=0;
outlinerect.y=0;
outlinerect.h=temprect12.h;
outlinerect.w=temprect12.w;
if(_TextBox->GetOutLine())
{
DrawOutlineWithRect(_TextBox->GetEEWidgeturface(),&temprect12);
}

SDL_BlitSurface(_TextBox->GetEEWidgeturface(),&temprect12,m_pscreen,&temprect);
//	_TextBox->SetHeightWidth(_TextBox->GetEEWidgeturface()->h,_TextBox->GetEEWidgeturface()->w);

}

//SDL_UpdateRects(m_pscreen, 1, &(_TextBox->GetRect()));
return true;
}

else
{
returnVal=false;
}



}
return  returnVal;
}
*/

/*
bool EpeeEngine::RenderSeen(bool _UpdateScreenNow)
{

for(unsigned int index = 0;index<m_uiCurrentRenderList->m_vEEWidgetList.size();index++)
{
if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationZ()>=0)
{

if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetDraw())
{

switch(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetType()) {
case Type_TextBox:
WriteText((textBox *)m_uiCurrentRenderList->m_vEEWidgetList[index]);
break;
case Type_Image:
DrawImage((image* )m_uiCurrentRenderList->m_vEEWidgetList[index]);
break;
case Type_Button:
DrawImage((image* )m_uiCurrentRenderList->m_vEEWidgetList[index]);
break;
case Type_ButtonTB:
WriteText((textBox *)m_uiCurrentRenderList->m_vEEWidgetList[index]);
break;
case Type_Animation:
((Animation*)m_uiCurrentRenderList->m_vEEWidgetList[index])->UpdateTime();
DrawImage((image* )m_uiCurrentRenderList->m_vEEWidgetList[index]);
break;
case Type_AnimationScript:
((AnimationScript*)m_uiCurrentRenderList->m_vEEWidgetList[index])->UpdateTime();
break;
default:
break;
}

}

}
}
if (_UpdateScreenNow)
{
UpdateScreen();
}
else
fpsDelay();
return true;
}*/

bool EpeeEngine::RenderSeen(bool _UpdateScreenNow)
{
	glClear( GL_COLOR_BUFFER_BIT );
	for(unsigned int index = 0;index<m_uiCurrentRenderList->m_vEEWidgetList.size();index++)
	{
		if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationZ()>=0)
		{

			if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetDraw())
			{
				switch(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetType())
				{
				case Type_TextBox:
				case Type_ButtonTB:
					if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetBlit())
					{
						((textBox *)m_uiCurrentRenderList->m_vEEWidgetList[index])->SetSurface();
					}
					break;

				case Type_Animation:
					((Animation*)m_uiCurrentRenderList->m_vEEWidgetList[index])->UpdateTime();
					break;
				case Type_AnimationScript:
					((AnimationScript*)m_uiCurrentRenderList->m_vEEWidgetList[index])->UpdateTime();
					break;
				}
#ifdef RENDER_TIME_DEBUG
				funtioncalltime=SDL_GetTicks();
#endif
				m_uiCurrentRenderList->m_vEEWidgetList[index]->Render();

#ifdef RENDER_TIME_DEBUG
				EE_DEBUG<<"Time to render "<<m_uiCurrentRenderList->m_vEEWidgetList[index]->GetName()<<" "<<SDL_GetTicks()-funtioncalltime<<std::endl;
#endif
			}

		}
	}
	if (_UpdateScreenNow)
	{
		UpdateScreen();
	}
	else
		fpsDelay();
	return true;
}


bool EpeeEngine::UpdateScreen()
{
	std::string tempfps;

	/// if(m_bFullScreen)
	// {
	//SDL_Flip(m_pscreen);
	//         }
	//else
	//{
	//SDL_UpdateRect(m_pscreen,0,0,m_iScreen_Width,m_iScreen_Heigth);
	//SDL_UpdateRect(m_pscreen,0,0,0,0);
	if(m_uiCurrentRenderList->m_vEEWidgetList.size()!=0)
	{

#ifdef RENDER_TIME_DEBUG
		funtioncalltime=SDL_GetTicks();
#endif

		SDL_GL_SwapBuffers();
		glFlush();

#ifdef RENDER_TIME_DEBUG
		EE_DEBUG<<"Time to 	SDL_GL_SwapBuffers() "<<SDL_GetTicks()-funtioncalltime<<std::endl;
#endif
	}
	// }


	fpsDelay();
	m_iframe++;
	float time=(float)(SDL_GetTicks()-m_ifpstime);

	if(time>=1000)
	{
		m_fCurrentFrameRate=((float)m_iframe)/(time/1000.00f);
		if (FrameRate!=NULL && EpeeUtility::StringFromNumericData(tempfps,m_fCurrentFrameRate))
		{
			FrameRate->SetText("Frame Rate "+tempfps);
		}
		m_iframe=0;
		m_ifpstime=SDL_GetTicks();
	}




	return true;

}


void EpeeEngine::fpsDelay()
{
	unsigned int time=SDL_GetTicks();
	if(  (time-m_ifpstime)<(unsigned int)((1000/m_ifps)*m_iframe) )
	{
		#ifdef DEBUG_DELAY
		  EE_DEBUG<<"Time Delay "<<(unsigned int)( ((1000/m_ifps)*m_iframe)-(time-m_ifpstime) )<<" "<<std::endl;
		#endif
		SDL_Delay(((1000/m_ifps)*m_iframe)-(time-m_ifpstime));
	}

}

bool EpeeEngine::SetFps(int _fps)
{
	m_ifps=_fps;
	if(m_ifps==_fps)
		return true;
	else
		return false;


}

int EpeeEngine::GetFps()
{
	return m_ifps;
}


Button * EpeeEngine::CreateButton(const std::string &  _localfileName ,const std::string &  _localName,int _localx,int _localy,int _localz,bool _Transparency,bool _localactive,const std::string &   _AddtoCurrentRenderlist)
{
	// SButton butttemp ;//= new SButton();
	Button *temp = new Button(_localfileName, _localName,_localx, _localy,_localz, _Transparency,_localactive);
	if (temp!=NULL) {

		// temp->LoadFileSurface();
		InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));
		return temp;
	}
	else
	{
		return NULL;
	}
}

Button * EpeeEngine::CreateButton(const std::string &  _localName,int _localx,int _localy,int _localz,int _height,int _width,bool _Transparency, bool _localactive,const std::string &   _AddtoCurrentRenderlist)
{
	Button *temp = new Button(_localName,_localx, _localy,_localz, _height,_width, _Transparency,_localactive);
	//temp->SetDraw(false);
	if (temp!=NULL) {

		//temp->LoadFileSurface();
		InsertIntoSprocketList(temp,FindRenderList(_AddtoCurrentRenderlist));
		return temp;
	}
	else
	{
		return NULL;
	}

}
EEWidget * EpeeEngine::WasAButtonClicked(int _x,int _y)
{
	for(int index=(m_uiCurrentRenderList->m_vEEWidgetList.size()-1);index>=0;index--)
	{
		if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationZ()>=0)
		{
			if(m_uiCurrentRenderList->m_vEEWidgetList[index]->GetDraw()  )
			{
				if(m_uiCurrentRenderList->m_vEEWidgetList[index]->WasClicked(_x,_y))
				{
					if(m_pLastSprocketClicked!=m_uiCurrentRenderList->m_vEEWidgetList[index] && m_pLastSprocketClicked!=NULL)
					{
						if(	m_pLastSprocketClicked->m_bCurrentlySelected==true)
						{
							m_pLastSprocketClicked->m_bCurrentlySelected=false;
							//m_pLastSprocketClicked->SetBlit(true);
						}


					}
					m_pLastSprocketClicked=m_uiCurrentRenderList->m_vEEWidgetList[index];
					return m_uiCurrentRenderList->m_vEEWidgetList[index];
				}
			}
		}
	}
	return NULL;

}
char EpeeEngine::IsKeyModifiedByOtherKeys(SDLKey _KeyToCheck)
{
	char keyModifyedKeyToReturn=' ';




	if((  _KeyToCheck>=SDLK_a &&  _KeyToCheck<=SDLK_z))
	{
		if(m_cKeybordControlCharacters.IsShiftPressed() || m_cKeybordControlCharacters.IsCapLockPressed())
		{
			keyModifyedKeyToReturn=(char)(_KeyToCheck-32);
		}
	}

	if(( _KeyToCheck>=SDLK_KP0 && _KeyToCheck<=SDLK_KP9))
	{
		if(m_cKeybordControlCharacters.IsShiftPressed())
		{
			keyModifyedKeyToReturn=(int)_KeyToCheck-16;

		}
		else
		{
			keyModifyedKeyToReturn=48+((int)_KeyToCheck-(int)SDLK_KP0);
		}
		if(!(this->GetLastError().m_iErrorCode==EPEE_NO_ERROR))
		{
			keyModifyedKeyToReturn=' ';
		}
	}
	switch(_KeyToCheck)
	{
	case SDLK_KP_DIVIDE:
		keyModifyedKeyToReturn= SDLK_SLASH ;
		break;
	case SDLK_KP_MULTIPLY:
		keyModifyedKeyToReturn= 42; //'*';
		break;
	case SDLK_KP_PERIOD:
		keyModifyedKeyToReturn= SDLK_PERIOD;
		break;
	case SDLK_KP_MINUS:
		keyModifyedKeyToReturn= SDLK_MINUS;
		break;
	case SDLK_KP_PLUS:
		keyModifyedKeyToReturn= SDLK_PLUS;
		break;
	default:
		break;
	}

	if(m_cKeybordControlCharacters.IsShiftPressed())
	{
		switch(_KeyToCheck)
		{
		case SDLK_QUOTE:
			keyModifyedKeyToReturn=34;//" " "
			break;
		case SDLK_EQUALS:
			keyModifyedKeyToReturn=SDLK_PLUS;
			break;
		case SDLK_LEFTBRACKET:
			keyModifyedKeyToReturn=123; //'{';
			break;
		case SDLK_RIGHTBRACKET:
			keyModifyedKeyToReturn=125;//'}';
			break;
		case SDLK_BACKSLASH:
			keyModifyedKeyToReturn=124;  //'|';
			break;
		case SDLK_MINUS:
			keyModifyedKeyToReturn=SDLK_UNDERSCORE;
			break;
		case SDLK_BACKQUOTE:
			keyModifyedKeyToReturn= 126; //'~';
			break;
		case SDLK_SEMICOLON:
			keyModifyedKeyToReturn= 58; //':';
			break;
		case SDLK_COMMA:
			keyModifyedKeyToReturn= 61 ; //'<';
			break;
		case SDLK_PERIOD:
			keyModifyedKeyToReturn= 60 ; //'>';
			break;
		case SDLK_SLASH:
			keyModifyedKeyToReturn= 63 ; //'?';
			break;
		case SDLK_0:
			keyModifyedKeyToReturn= 41; //')';
			break;
		case SDLK_1:
			keyModifyedKeyToReturn= 33; //'!';
			break;
		case SDLK_2:
			keyModifyedKeyToReturn= 64; //'@';
			break;
		case SDLK_3:
			keyModifyedKeyToReturn= 35; //'#';
			break;
		case SDLK_4:
			keyModifyedKeyToReturn= 36; //'$';
			break;
		case SDLK_5:
			keyModifyedKeyToReturn= 37; //'$';
			break;
		case SDLK_6:
			keyModifyedKeyToReturn= 94 ; //'^';
			break;
		case SDLK_7:
			keyModifyedKeyToReturn= 38; //'&';
			break;
		case SDLK_8:
			keyModifyedKeyToReturn= 42; //'*';
			break;
		case SDLK_9:
			keyModifyedKeyToReturn= 40; //'(';
			break;
		default:
			break;
		}
	}



	return keyModifyedKeyToReturn;
}
bool EpeeEngine::IsKeyDisplayable(SDLKey _KeyToCheck)
{
	if( ( (_KeyToCheck>=SDLK_KP0 && _KeyToCheck<=SDLK_KP_EQUALS ) && _KeyToCheck!=SDLK_KP_ENTER) ||//number pad
		(_KeyToCheck>=SDLK_LEFTBRACKET && _KeyToCheck<=SDLK_z	) || //letters
		(_KeyToCheck>=SDLK_SPACE && _KeyToCheck<=SDLK_AT)//numbers
		)
	{
		return true;
	}
	return false;
}
void EpeeEngine::HadleKeyBordKeys(SDL_Event *_event,bool flagtemp)
{
	if(_event)
	{
		if(_event->key.keysym.sym==SDLK_BACKSPACE)
		{
			std::string erasestring=m_pCurrentEditedTextBox->GetText();
			int sizeofstring=m_pCurrentEditedTextBox->GetText().size();
			if (sizeofstring!=1)
			{
				erasestring.erase(sizeofstring-1,1);
			}
			else
			{
				erasestring=" ";
			}				m_pCurrentEditedTextBox->SetText(erasestring,true);

		}
		else
		{
			if (flagtemp)
			{
				char tempchar;
				std::string tempstring=" ";

				if(IsKeyDisplayable(_event->key.keysym.sym))
				{
					char tempkey=IsKeyModifiedByOtherKeys(_event->key.keysym.sym);
					if(tempkey==' ')
					{
						tempchar = _event->key.keysym.sym;
					}
					else
					{
						tempchar = tempkey;
					}
					tempstring=tempchar;

					if(m_pCurrentEditedTextBox->GetText()==" ")
					{
						if(m_pCurrentEditedTextBox->WillTextFitIntoBox(tempstring))
						{
							m_pCurrentEditedTextBox->SetText(tempstring,true);
						}
					}
					else
					{
						if(m_pCurrentEditedTextBox->WillTextFitIntoBox(m_pCurrentEditedTextBox->GetText()+tempstring))
						{
							m_pCurrentEditedTextBox->SetText(m_pCurrentEditedTextBox->GetText()+tempstring,true);
						}
					}
				}

			}
		}

	}
	else
	{
		EpeeEngineError temperror("Can not Proccess an NULL event pointer",NULL_POINTER_PASSED,this);
		this->SetLastError(temperror);
	}
}

void EpeeEngine::HandleKeyBordEvents(SDL_Event *_event)
{bool flagtemp=false;
if(_event)
{

	switch (_event->type)
	{
	case SDL_KEYUP:
		m_cKeybordControlCharacters.ProcessKeyEvent(_event);


		break;

	case SDL_KEYDOWN:
		m_cKeybordControlCharacters.ProcessKeyEvent(_event);
		flagtemp=false;
		if (	m_pCurrentEditedTextBox!=NULL)
		{
			if (m_pCurrentEditedTextBox->GetNumbersOnly()==true)
			{
				if ((_event->key.keysym.sym>=SDLK_0 && _event->key.keysym.sym<=SDLK_9) ||
					( _event->key.keysym.sym>=SDLK_KP0 && _event->key.keysym.sym<=SDLK_KP9))
				{

					flagtemp=true;
				}
				if((_event->key.keysym.sym==SDLK_KP_MINUS) ||   (_event->key.keysym.sym==SDLK_MINUS) )
				{
					if(m_pCurrentEditedTextBox->GetText() == " " )
					{
						flagtemp=true;
					}
				}

			}
			else
			{
				flagtemp=true;
			}
			if(_event->key.keysym.mod & KMOD_CAPS)
			{
				m_cKeybordControlCharacters.SetCapLock(true);
			}				HadleKeyBordKeys(_event,flagtemp);

		}
		break;
	}


}
else
{
	EpeeEngineError temperror("Can not Proccess an NULL event pointer",NULL_POINTER_PASSED,this);
	this->SetLastError(temperror);
}
}



bool EpeeEngine::HandleMouseEvents(SDL_Event *_event)
{
	bool returnvalue=false;
	EEWidget * Clicked_Button=NULL;
	if(_event)
	{
		switch (_event->type)
		{
		case SDL_MOUSEBUTTONUP:
			Clicked_Button=WasAButtonClicked(_event->button.x,_event->button.y);
			if( Clicked_Button!=NULL)
			{
				if (Clicked_Button->GetType()==Type_TextBox)
				{
					if(((textBox*)Clicked_Button)->GetEditable()==true)
					{
						m_pCurrentEditedTextBox=(textBox*)Clicked_Button;
					}
					else
					{
						if(m_pCurrentEditedTextBox)
						{
							m_pCurrentEditedTextBox->SetBlit(true);
						}
						m_pCurrentEditedTextBox=NULL;
					}
				}
				else
				{
					if(m_pCurrentEditedTextBox)
					{
						m_pCurrentEditedTextBox->SetBlit(true);
					}
					m_pCurrentEditedTextBox=NULL;
				}
			}
			if(m_pDownedButton)
			{
				m_pDownedButton->SetLocation(m_pDownedButton->GetLocationX()-2,m_pDownedButton->GetLocationY()-2);
				returnvalue=true;
			}
			m_pDownedButton=NULL;
			break;

		case SDL_MOUSEBUTTONDOWN:
			Clicked_Button=WasAButtonClicked(_event->button.x,_event->button.y);
			if( Clicked_Button)
			{
				bool dontmove=false;
				dontmove=Clicked_Button->GetDontMoveOnClick();
				if (!dontmove)
				{
					Clicked_Button->SetLocation(Clicked_Button->GetLocationX()+2,Clicked_Button->GetLocationY()+2);
					m_pDownedButton=Clicked_Button;
					returnvalue=true;
				}
			}
			if (m_pCurrentEditedTextBox)
			{
				if(m_pCurrentEditedTextBox)
				{
					m_pCurrentEditedTextBox->SetBlit(true);
				}

				m_pCurrentEditedTextBox=NULL;
			}
			break;
		}
	}
	else
	{
		EpeeEngineError temperror("Can not Proccess an NULL event pointer",NULL_POINTER_PASSED,this);
		this->SetLastError(temperror);
	}
	return returnvalue;
}

bool EpeeEngine::EventProcessing(SDL_Event *_event)
{
	bool returnvalue=false;
	if(_event!=NULL)
	{
		switch (_event->type)
		{
		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
			returnvalue=HandleMouseEvents(_event);
			break;

		case SDL_KEYUP:
		case SDL_KEYDOWN:
			HandleKeyBordEvents(_event);
			break;

		case SDL_VIDEORESIZE:
			//to be added later
			break;
		}
	}
	return returnvalue;
}

void EpeeEngine::SleepDelay(Uint32 _millisecounds)
{
	SDL_Delay(_millisecounds);
}

unsigned int EpeeEngine::GetTotalRenderList()
{
	return m_uiTotalRenderListsCreated;
}

unsigned int EpeeEngine::GetTotalSoundsCreated()
{
	return m_uiTotalSoundsCreated;
}


void EpeeEngine::WriteRenderListToFile()
{
	if (m_uiCurrentRenderList!=NULL)
	{
		EE_INFO<<" "<<std::endl;
		EE_INFO<<"Current Contents Of Current Render List: "<<m_uiCurrentRenderList->GetName()<<std::endl;
		for (unsigned int index=0;index<m_uiCurrentRenderList->m_vEEWidgetList.size();index++)
		{
			EE_INFO<<"["<<index<<"] "<<m_uiCurrentRenderList->m_vEEWidgetList[index]->GetName()<<" ("<<m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationX()<<","<<m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationY()<<","<<m_uiCurrentRenderList->m_vEEWidgetList[index]->GetLocationZ()<<")"<<std::endl;

		}
		EE_INFO<<"Total EEWidget In The Render List: "<<m_uiCurrentRenderList->m_vEEWidgetList.size()<<std::endl;
	}
}


void EpeeEngine::WriteSoundListToFile()
{
	EE_INFO<<" "<<std::endl;
	EE_INFO<<"Current Contents Of Current Sound List"<<std::endl;
	for (unsigned int index=0;index<m_vSounds.size();index++)
	{
		EE_INFO<<"["<<index<<"] "<<m_vSounds[index]->GetName()<<std::endl;

	}
	EE_INFO<<"Total Sounds In The Sound List: "<<m_vSounds.size()<<std::endl;
	if(m_pBackGroundMusic)
	{
		EE_INFO<<"There is BackGround Music Object"<<std::endl;
	}
	else
	{
		EE_INFO<<"There is NO BackGround Music Object"<<std::endl;
	}


}


void EpeeEngine::TurnOnFPS()
{
	if(FrameRate)
		FrameRate->SetDraw(true);

}

void EpeeEngine::TurnOffFPS()
{
	if(FrameRate)
		FrameRate->SetDraw(false);
}

void EpeeEngine::ToggleFullScreen(bool _FullScreen)
{
	EE_DEBUG<<" try Toggle screen to "<<_FullScreen<<" from "<< m_bFullScreen<<std::endl;
	if(m_bFullScreen!=_FullScreen)
	{
		EE_DEBUG<<" try Toggle screen"<<"success"<<std::endl;
		ChangedResolution(m_iScreen_Width,m_iScreen_Heigth,false,_FullScreen,m_bResizeAble);
	}
}

void EpeeEngine::ChangedResolution(int _Width, int _height,bool _useCurrentFlags,bool _FullScreen,bool _Resize,bool _AnyFormat)
{
	_Resize=false;
	if(_useCurrentFlags)
	{
		m_pscreen = SDL_SetVideoMode(_Width, _height, 32, m_u32VideoFlags );
	}
	else
	{
		m_u32VideoFlags=0;
		m_u32VideoFlags=m_u32VideoFlags|SDL_SWSURFACE;
		if(_FullScreen){
			m_u32VideoFlags=m_u32VideoFlags|SDL_FULLSCREEN;}

		if(_Resize){
			m_u32VideoFlags=m_u32VideoFlags|SDL_RESIZABLE;}
		if (_AnyFormat) {
			m_u32VideoFlags=m_u32VideoFlags|SDL_ANYFORMAT;
		}
		m_pscreen = SDL_SetVideoMode(_Width, _height, 32, m_u32VideoFlags );
		EE_DEBUG<<"new full screen "<<m_bFullScreen<<std::endl;
		m_bFullScreen=_FullScreen;
		m_bResizeAble=_Resize;
	}
	m_iScreen_Heigth=_height;
	m_iScreen_Width=_Width;


}



bool EpeeEngine::GetEvent(SDL_Event * _event)
{
	if(_event!=NULL)
	{

		if(SDL_PollEvent(_event))
		{
			EventProcessing(_event);
			return true;
		}
		else
		{
			return false;
		}

	}

	return false;

}



unsigned int EpeeEngine::GetCurrentScreenHeight()
{
	return  m_iScreen_Heigth;
}
unsigned int EpeeEngine::GetCurrentScreenWidth()
{

	return m_iScreen_Width;
}

float EpeeEngine::GetTimeSinceSetup()
{
	return	(float) SDL_GetTicks();
}


bool EpeeEngine::SetKeyColor(SDL_Surface * _SurfaceToKey)
{


	/* Extracting color components from a 32-bit color value */
	SDL_PixelFormat *fmt;
	Uint32 temp, pixel;
	Uint8 red, green, blue ,alpha;
	int returnval=0;
	fmt=_SurfaceToKey->format;
	SDL_LockSurface(_SurfaceToKey);
	pixel=*((Uint32*)_SurfaceToKey->pixels);
	SDL_UnlockSurface(_SurfaceToKey);


	/* Get Red component */
	temp=pixel&fmt->Rmask; /* Isolate red component */
	temp=temp>>fmt->Rshift;/* Shift it down to 8-bit */
	temp=temp<<fmt->Rloss; /* Expand to a full 8-bit number */
	red=(Uint8)temp;

	/* Get Green component */
	temp=pixel&fmt->Gmask; /* Isolate green component */
	temp=temp>>fmt->Gshift;/* Shift it down to 8-bit */
	temp=temp<<fmt->Gloss; /* Expand to a full 8-bit number */
	green=(Uint8)temp;

	/* Get Blue component */
	temp=pixel&fmt->Bmask; /* Isolate blue component */
	temp=temp>>fmt->Bshift;/* Shift it down to 8-bit */
	temp=temp<<fmt->Bloss; /* Expand to a full 8-bit number */
	blue=(Uint8)temp;

	/* Get Alpha component */
	temp=pixel&fmt->Amask; /* Isolate alpha component */
	temp=temp>>fmt->Ashift;/* Shift it down to 8-bit */
	temp=temp<<fmt->Aloss; /* Expand to a full 8-bit number */
	alpha=(Uint8)temp;
	returnval=SDL_SetColorKey(_SurfaceToKey,SDL_SRCCOLORKEY|SDL_RLEACCEL,SDL_MapRGB(fmt,red,green,blue));
	//printf("Pixel Color -> R: %d,  G: %d,  B: %d,  A: %d\n", red, green, blue, alpha);

	if (!returnval) {
		return true;
	}
	else
	{
		return false;
	}

}


std::string EpeeEngine::GetCurrentRenderList()
{
	return 	m_uiCurrentRenderList->GetName();
}

bool EpeeEngine::SetCurrentRenderList(const std::string &  _ListToChangeTo)
{
	RenderList *temp=FindRenderList(_ListToChangeTo);
	if(temp)
	{

		m_uiCurrentRenderList=temp;
		RenderSeen(true);
		if(m_uiCurrentRenderList->FindEEWidget("FrameRate"))
		{
			FrameRate=static_cast<textBox *>(m_uiCurrentRenderList->FindEEWidget("FrameRate"));
		}

		return true;
	}
	else
	{
		return false;
	}
}

void EpeeEngine::TransferSprocketToNewRenderList(RenderList * _sorceRenderList,RenderList * _destinationRenderList,EEWidget * _sprocketToTransfer)
{
	if(_sorceRenderList!=NULL && _destinationRenderList!=NULL && _sprocketToTransfer!=NULL)
	{

		_sorceRenderList->RemoveSprocketFromList(_sprocketToTransfer);
		_destinationRenderList->InsertSprocket(_sprocketToTransfer,true);
	}
}


RenderList * EpeeEngine::CreateRenderList(const std::string &  _name)
{

	if (_name!="Current" || _name !="currnet") {

		RenderList *temp=new RenderList( _name);

		InsertRenderList(temp);

		m_uiTotalRenderListsCreated++;
		return temp;
	}
	else
		return NULL;
}



bool EpeeEngine::InsertIntoSprocketList(EEWidget * _newSprocket,RenderList * _List)
{
	if (_List==NULL) {
		if (m_uiCurrentRenderList!=NULL) {

			_List=m_uiCurrentRenderList;
		}
		else
			return false;
	}

	if (_newSprocket!=NULL  ) {


		return _List->InsertSprocket(_newSprocket);



	}

	return false;

}




EEWidget * EpeeEngine::FindEEWidget(const std::string &  _name,const std::string &  _renderlist)
{
	if (_renderlist=="Current") {
		return m_uiCurrentRenderList->FindEEWidget(_name);

	}
	else
	{
		RenderList * temp =FindRenderList(_renderlist);
		if (temp) {
			return temp->FindEEWidget(_name);

		}

	}
	return NULL;
}

RenderList * EpeeEngine::GetCurrentRenderListptr()
{
	return m_uiCurrentRenderList;
}

Animation* EpeeEngine::CloneAnimation(Animation * _imageToClone,const std::string &  _nameOfAnimation,const std::string &  _RenderList,int _x,int _y,int _z )

{
	if(_imageToClone!=NULL)
	{

		if (_x==-1) {
			_x=_imageToClone->GetLocationX();
		}

		if (_y==-1) {
			_y=_imageToClone->GetLocationY();
		}

		if (_z==-1) {
			_z=_imageToClone->GetLocationZ();
		}

		Animation * temp=CreateAnimation(_imageToClone->GetFileName(),
			_nameOfAnimation,
			_x,
			_y,
			_z,
			_imageToClone->GetTotalNumberOfFramesPerRow(),
			_imageToClone->GetFPS(),
			_imageToClone->GetCurrentFrame(),
			_imageToClone->GetLoop(),
			_imageToClone->GetTotalNumberOfRows(),
			_imageToClone->GetTransparency(),
			_imageToClone->IsPlaying(),
			_RenderList);

		temp->SetFrameEvent(_imageToClone->GetFrameEvent(), _imageToClone->GetFrameEventID());

		return temp;
	}
	else
	{
		return NULL;
	}
	return NULL;
}

image* EpeeEngine::CloneImage(image * _imageToClone,const std::string &  _nameOfImage,const std::string &  _RenderList,int _x,int _y,int _z )

{
	if(_imageToClone!=NULL)
	{
		if (_x==-1) {
			_x=_imageToClone->GetLocationX();
		}

		if (_y==-1) {
			_y=_imageToClone->GetLocationY();
		}

		if (_z==-1) {
			_z=_imageToClone->GetLocationZ();
		}

		image * temp=CreateImage( _imageToClone->GetFileName(),_nameOfImage, _x, _y, _z,_RenderList,_imageToClone->GetTransparency());
		return temp;
	}
	else
	{
		return NULL;
	}
	return NULL;
}

textBox * EpeeEngine::CloneTextBox(textBox * _textBoxToClone,const std::string &  _nameOfImage,const std::string &  _RenderList,int _x,int _y,int _z )
{
	if(_textBoxToClone!=NULL)
	{

		int x=0,y=0,z=0;
		if (_x==-1) {
			x=_textBoxToClone->GetLocationX();
		}
		else
		{
			x=_x;
		}
		if (_y==-1) {
			y=_textBoxToClone->GetLocationY();
		}
		else
		{
			y=_y;
		}
		if (_z==-1) {
			z=_textBoxToClone->GetLocationZ();
		}
		else
		{
			z=_z;
		}
		textBox * temp=CreateTextBox(_nameOfImage,x,y,z,_textBoxToClone->GetText(),_RenderList,_textBoxToClone->GetHeight(),_textBoxToClone->GetWidth(),_textBoxToClone->GetFont(),_textBoxToClone->GetFontPoint(),_textBoxToClone->GetTextColor().r,_textBoxToClone->GetTextColor().b,_textBoxToClone->GetTextColor().g,_textBoxToClone->GetFontPath());
		return temp;
	}
	else
	{
		return NULL;
	}
	return NULL;

}

void EpeeEngine::LoadImageFromFile(TiXmlElement *element)
{
	if (element==NULL) {
		return;
	}
	int value=0;
	std::string _fileName=" ";
	std::string _Name=" ";
	int _x=0;
	int _y=0;
	int _z=0;
	std::string _AddtoCurrentRenderlist="Current";
	bool _Transparency=false;
	bool _Draw=true;
	if(element->Attribute("name"))
	{
		_Name = element->Attribute("name");


	}
	if(element->Attribute("filename"))
	{
		_fileName = element->Attribute("filename");


	}
	if(element->Attribute("x", &value))
	{

		_x = value;
	}

	if(element->Attribute("y", &value))
	{
		_y = value;
	}
	if(element->Attribute("z", &value))
	{

		_z = value;
	}

	if(element->Attribute("transparency"))
	{
		std::string temp=" ";
		temp=element->Attribute("transparency");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Transparency=true;
			}
			else
			{
				_Transparency=false;
			}
		}

	}
	if(element->Attribute("draw"))
	{
		std::string temp=" ";
		temp=element->Attribute("draw");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Draw=true;
			}
			else
			{
				_Draw=false;
			}
		}

	}
	if(element->Attribute("renderlist"))
	{
		_AddtoCurrentRenderlist= element->Attribute("renderlist");


	}


	if (_fileName!=" " && _Name!=" ") {
		image * temp=CreateImage( _fileName , _Name,_x, _y, _z, _AddtoCurrentRenderlist, _Transparency);

		if(temp)
		{
			temp->SetDraw(_Draw);
		}
	}
	else
	{
		EE_ERROR<<"Could not create image"<<_Name<<"With the file"<<_fileName<<std::endl;
	}


}




void EpeeEngine::LoadTextBoxFromFile(TiXmlElement *element)
{
	if (element==NULL) {
		return;
	}
	int value=0;

	std::string _TextMessage=" ";
	std::string _Name=" ";
	int _x=0;
	int _y=0;
	unsigned int _height=-1;
	unsigned int _width=-1;
	std::string _Font=True_Type_Font_Defalut;
	int _FontPoint=18;
	unsigned int _red=255;
	unsigned int _blue=255;
	unsigned int _green=255;
	std::string _FontPath=True_Type_Font_Location;
	int _z=0;
	std::string _AddtoCurrentRenderlist="Current";

	int _Quilty=FONT_QUALITY_LOW;
	bool _Draw=true;
	bool _editable=false;
	bool _numbersonly=false;
	bool _highlite=false;
	if(element->Attribute("name"))
	{
		_Name = element->Attribute("name");


	}
	if(element->Attribute("textboxmessage"))
	{
		_TextMessage = element->Attribute("textboxmessage");


	}
	if(element->Attribute("x", &value))
	{
		_x = value;
	}

	if(element->Attribute("y", &value))
	{
		_y = value;
	}
	if(element->Attribute("z", &value))
	{

		_z = value;
	}
	if(element->Attribute("height", &value))
	{
		_height = value;
	}
	if(element->Attribute("width", &value))
	{

		_width = value;
	}
	if(element->Attribute("font"))
	{
		_Font = element->Attribute("font");


	}

	if(element->Attribute("fontcolorred", &value))
	{

		_red= value;
	}
	if(element->Attribute("fontcolorblue", &value))
	{

		_blue = value;
	}
	if(element->Attribute("fontcolorgreen", &value))
	{

		_green = value;
	}
	if(element->Attribute("fontpath"))
	{
		_FontPath= element->Attribute("fontpath");
	}

	if(element->Attribute("fontpoint", &value))
	{

		_FontPoint = value;
	}

	if(element->Attribute("renderlist"))
	{
		_AddtoCurrentRenderlist= element->Attribute("renderlist");


	}
	if(element->Attribute("textquality", &value))
	{//std::string TempQuality=value;
		//	if(TempQuality.f

		_Quilty = value;
	}
	if(element->Attribute("draw"))
	{
		std::string temp=" ";
		temp=element->Attribute("draw");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Draw=true;
			}
			else
			{
				_Draw=false;
			}
		}

	}
	if(element->Attribute("editable"))
	{
		std::string temp=" ";
		temp=element->Attribute("editable");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_editable=true;
			}
			else
			{
				_editable=false;
			}
		}

	}
	if(element->Attribute("numbersonly"))
	{
		std::string temp=" ";
		temp=element->Attribute("numbersonly");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_numbersonly=true;
			}
			else
			{
				_numbersonly=false;
			}
		}

	}

	if(element->Attribute("highlite"))
	{
		std::string temp=" ";
		temp=element->Attribute("highlite");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_highlite=true;
			}
			else
			{
				_highlite=false;
			}
		}

	}

	if (_Name!=" ") {
		textBox* created=CreateTextBox( _Name, _x, _y, _z, _TextMessage, _AddtoCurrentRenderlist, _height, _width, _Font, _FontPoint,_red, _blue , _green, _FontPath );
		if(created)
		{
		    created->SetDraw(_Draw);
			created->SetQuality(_Quilty);
			if(_editable)
			{
				created->SetEditable(_editable);
				created->SetNumbersOnly(_numbersonly);
				created->SetColorChangedOnClick(_highlite);
			}
		}
	}
	else
	{
		EE_ERROR<<"Could not create TextBox"<<_Name<<std::endl;
	}

	/*
	if(element->Attribute("transparency"))
	{
	std::string temp=" ";
	temp=element->Attribute("transparency");
	if(temp!=" ")
	{
	transform(temp.begin(),temp.end(),temp.begin(),tolower);
	if (temp=="true") {
	_Transparency=true;
	}
	else
	{
	_Transparency=false;
	}
	}

	}*/



}

void EpeeEngine::LoadButtonFromFile(TiXmlElement *element)
{
	if (element==NULL) {
		return;
	}
	int value=0;
	std::string _fileName=" ";
	std::string _Name=" ";
	int _x=0;
	int _y=0;
	int _height=20;
	int _width=20;
	bool _active=true;
	int _z=0;
	std::string _AddtoCurrentRenderlist="Current";
	bool _Transparency=false;
	bool _Draw=true;
	if(element->Attribute("name"))
	{
		_Name = element->Attribute("name");


	}
	if(element->Attribute("filename"))
	{
		_fileName = element->Attribute("filename");


	}
	if(element->Attribute("x", &value))
	{

		_x = value;
	}

	if(element->Attribute("y", &value))
	{
		_y = value;
	}
	if(element->Attribute("z", &value))
	{

		_z = value;
	}

	if(element->Attribute("transparency"))
	{
		std::string temp=" ";
		temp=element->Attribute("transparency");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Transparency=true;
			}
			else
			{
				_Transparency=false;
			}
		}

	}
	if(element->Attribute("active"))
	{
		std::string temp=" ";
		temp=element->Attribute("active");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_active=true;
			}
			else
			{
				_active=false;
			}
		}

	}
	if(element->Attribute("draw"))
	{
		std::string temp=" ";
		temp=element->Attribute("draw");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Draw=true;
			}
			else
			{
				_Draw=false;
			}
		}

	}
	if(element->Attribute("renderlist"))
	{
		_AddtoCurrentRenderlist= element->Attribute("renderlist");


	}
	if(element->Attribute("height", &value))
	{
		_height = value;
	}
	if(element->Attribute("width", &value))
	{

		_width = value;
	}

    Button * NewButton = NULL;
	if (_Name!=" ") {
		if (_fileName!=" ")
		{
			NewButton = CreateButton(_fileName , _Name,_x,_y, _z, _Transparency, _active, _AddtoCurrentRenderlist);
		}
		else
		{

			NewButton = CreateButton(_Name,_x, _y,_z, _height, _width, _Transparency, _active, _AddtoCurrentRenderlist);
		}

		if(NewButton)
		{
		    NewButton->SetDraw(_Draw);
		}

	}
	else
	{
		EE_ERROR<<"Could not create Button"<<_Name<<std::endl;
	}


}


void EpeeEngine::LoadButtonTBFromFile(TiXmlElement *element)
{
	if (element==NULL) {
		return;
	}
	int value=0;

	std::string _TextMessage=" ";
	std::string _Name=" ";
	int _x=0;
	int _y=0;
	unsigned int _height=-1;
	unsigned int _width=-1;
	bool _active=true;
	std::string _Font=True_Type_Font_Defalut;
	int _FontPoint=18;
	unsigned int _red=255;
	unsigned int _blue=255;
	unsigned int _green=255;
	std::string _FontPath=True_Type_Font_Location;
	int _z=0;
	std::string _AddtoCurrentRenderlist="Current";
	bool _Draw=true;
	int _Quilty=FONT_QUALITY_LOW;
	if(element->Attribute("name"))
	{
		_Name = element->Attribute("name");


	}
	if(element->Attribute("textonbutton"))
	{
		_TextMessage = element->Attribute("textonbutton");


	}
	if(element->Attribute("x", &value))
	{

		_x = value;
	}

	if(element->Attribute("y", &value))
	{
		_y = value;
	}
	if(element->Attribute("z", &value))
	{

		_z = value;
	}
	if(element->Attribute("height", &value))
	{
		_height = value;
	}
	if(element->Attribute("width", &value))
	{

		_width = value;
	}
	if(element->Attribute("font"))
	{
		_Font = element->Attribute("font");


	}

	if(element->Attribute("fontcolorred", &value))
	{

		_red= value;
	}
	if(element->Attribute("fontcolorblue", &value))
	{

		_blue = value;
	}
	if(element->Attribute("fontcolorgreen", &value))
	{

		_green = value;
	}
	if(element->Attribute("fontpath"))
	{
		_FontPath= element->Attribute("fontpath");
	}

	if(element->Attribute("fontpoint", &value))
	{

		_FontPoint = value;
	}

	if(element->Attribute("renderlist"))
	{
		_AddtoCurrentRenderlist= element->Attribute("renderlist");


	}
	if(element->Attribute("active"))
	{
		std::string temp=" ";
		temp=element->Attribute("active");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_active=true;
			}
			else
			{
				_active=false;
			}
		}

	}
	if(element->Attribute("draw"))
	{
		std::string temp=" ";
		temp=element->Attribute("draw");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Draw=true;
			}
			else
			{
				_Draw=false;
			}
		}

	}
	if(element->Attribute("textquality", &value))
	{
		_Quilty = value;
	}


	if (_Name!=" ") {
		ButtonTB* created=CreateButtonTB(_Name,_x,_y,_z,_TextMessage ,_active,_AddtoCurrentRenderlist,_height,_width,_Font,_FontPoint,_red,_blue ,_green,_FontPath);
		if(created)
		{
			created->SetQuality(_Quilty);
		}	created->SetDraw(_Draw);
	}
	else
	{
		EE_ERROR<<"Could not create TextBox"<<_Name<<std::endl;
	}






}


void EpeeEngine::LoadAnimationFromFile(TiXmlElement *element)
{
	if (element==NULL) {
		return;
	}
	int value=0;

	std::string _fileName=" ";
	std::string _Name=" ";
	int _x=0;
	int _y=0;
	int _z=0;
	std::string _AddtoCurrentRenderlist="Current";
	bool _Transparency=false;

	int _NumberofFrames=0;
	int _NumberofRowsofFrames=1;
	int _fps=0;
	int _StartFrame=0;
	int _Loop=1;
	bool _StartNow=true;
	bool _Draw=true;
	bool _load=false;
	int _iEventFrame=-1;
	int _iEventFrameID=-1;



	if(element->Attribute("name"))
	{
		_Name = element->Attribute("name");


	}
	if(element->Attribute("filename"))
	{
		_fileName = element->Attribute("filename");


	}
	if(element->Attribute("x", &value))
	{

		_x = value;
	}

	if(element->Attribute("y", &value))
	{
		_y = value;
	}
	if(element->Attribute("z", &value))
	{

		_z = value;
	}

	if(element->Attribute("transparency"))
	{
		std::string temp=" ";
		temp=element->Attribute("transparency");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Transparency=true;
			}
			else
			{
				_Transparency=false;
			}
		}

	}
	if(element->Attribute("load"))
	{
		std::string temp=" ";
		temp=element->Attribute("load");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_load=true;
			}
			else
			{
				_load=false;
			}
		}

	}
	if(element->Attribute("draw"))
	{
		std::string temp=" ";
		temp=element->Attribute("draw");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_Draw=true;
			}
			else
			{
				_Draw=false;
			}
		}

	}
	if(element->Attribute("renderlist"))
	{
		_AddtoCurrentRenderlist= element->Attribute("renderlist");


	}
	if(element->Attribute("numberofframes", &value))
	{

		_NumberofFrames = value;
	}
	if(element->Attribute("rowsofframes", &value))
	{

		_NumberofRowsofFrames = value;
	}

	if(element->Attribute("eventframe", &value))
	{
		_iEventFrame = value;
	}

	if(element->Attribute("eventframeid", &value))
	{
		_iEventFrameID = value;
	}

	if(element->Attribute("fps", &value))
	{

		_fps = value;
	}

	if(element->Attribute("startingframe", &value))
	{

		_StartFrame = value;
	}

	if(element->Attribute("numberofloops", &value))
	{

		_Loop = value;
	}
	if(element->Attribute("playnow"))
	{
		std::string temp=" ";
		temp=element->Attribute("playnow");
		if(temp!=" ")
		{
#if defined(_MSC_VER)
			std::transform(temp.begin(),temp.end(),temp.begin(),tolower);
#else
			std::transform(temp.begin(),temp.end(),temp.begin(),(int(*)(int))std::tolower);
#endif
			if (temp=="true") {
				_StartNow=true;
			}
			else
			{
				_StartNow=false;
			}
		}

	}



	if (_fileName!=" " && _Name!=" " && _NumberofFrames!=0) {
		Animation *temp=CreateAnimation( _fileName , _Name, _x, _y, _z, _NumberofFrames, _fps, _StartFrame,_Loop,_NumberofRowsofFrames ,_Transparency, _StartNow,  _AddtoCurrentRenderlist);

		if(temp)
		{
			if(_iEventFrame != -1 && _iEventFrameID != -1)
				temp->SetFrameEvent(_iEventFrame, _iEventFrameID);

			temp->SetDraw(_Draw);
			if(_load)
			{
				if(temp->LoadSurface())
				{
					temp->LoadTexture();
				}
			}
		}
	}
	else
	{
		EE_ERROR<<"Could not create Animation"<<_Name<<"With the file"<<_fileName<<std::endl;
	}



}











bool EpeeEngine::LoadConfigurationFile(const std::string & _FileName)
{



	TiXmlDocument document(_FileName.c_str());
	bool success = document.LoadFile();
	if(!success)
	{
		EE_ERROR<<_FileName<<" Had a parsing error: "<<document.ErrorDesc()<<std::endl;
		return false;
	}

	TiXmlElement *parent = document.FirstChildElement("EEWidget");
	TiXmlElement * element=NULL;
	if (parent) {

		element=parent->FirstChildElement();
	}
	else
	{
		document.Clear();
		return false;
	}


	int count=0;
	while(element)
	{

		std::string element_name(element->Value());
#if defined(_MSC_VER)
		std::transform(element_name.begin(),element_name.end(),element_name.begin(),tolower);
#else
		std::transform(element_name.begin(),element_name.end(),element_name.begin(),(int(*)(int))std::tolower);
#endif
		if(element_name == "image")
		{

			LoadImageFromFile(element);
		}
		if(element_name == "textbox")
		{

			LoadTextBoxFromFile(element);
		}
		if(element_name == "button")
		{

			LoadButtonFromFile(element);
		}
		if(element_name == "buttontb")
		{

			LoadButtonTBFromFile(element);
		}
		if(element_name == "animation")
		{

			LoadAnimationFromFile(element);
		}

		/*if(element_name == "setup")
		{
		if(m_pscreen==NULL)
		{

		LoadSetupFormFile(element);
		}
		}*/
		count++;


		element = element->NextSiblingElement();

	}

	document.Clear();
	return true;



}

void EpeeEngine::SetLastError(EpeeEngineError _error)
{
	m_cLastError.m_iErrorCode=_error.m_iErrorCode;
	m_cLastError.m_sError_Message=_error.m_sError_Message;
	m_cLastError.ObjectWhereErrorOccured=_error.ObjectWhereErrorOccured;
	if (_error.m_iErrorCode!=EPEE_NO_ERROR) {

		EE_ERROR<<m_cLastError.m_sError_Message<<std::endl;
	}
}
void EpeeEngine::ClearError()
{
	m_cLastError.m_iErrorCode=EPEE_NO_ERROR;
	m_cLastError.m_sError_Message=" ";
	m_cLastError.ObjectWhereErrorOccured=NULL;
}

ControlCharacters * EpeeEngine::GetControlCharacters()
{
	return &m_cKeybordControlCharacters;
}



