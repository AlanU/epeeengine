/*
*  EpeeEngineCommenIncludes.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#pragma once
#ifndef EPEEENGINECOMMENINCLUDES_H
#define EPEEENGINECOMMENINCLUDES_H

#if defined (_WIN32)

//#include <windows.h>
#include "SDL.h"
#include "SDL_thread.h"
#include "SDL_opengl.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"
#include "SDL_keysym.h"
#include "SDL_image.h"
#define True_Type_Font_Location "C:\\WINDOWS\\Fonts\\" //windows
#define True_Type_Font_Defalut  "ARIAL.TTF" //windows
#define EE_OPEN_GL_TEXTURE_TYPE GL_UNSIGNED_BYTE
#endif

#if defined (__APPLE__)

#include "SDL/SDL.h"
#include "SDL/SDL_thread.h"
#include "SDL_ttf/SDL_ttf.h"
#include "SDL/SDL_keysym.h"
#include "SDL/SDL_opengl.h"
#include "SDL_mixer/SDL_mixer.h"
#include "SDL_image/SDL_image.h"
#define True_Type_Font_Location "/Library/Fonts/"   //Macintosh
#define True_Type_Font_Defalut  "Apple LiGothic Medium.ttf" //Macintosh
#define EE_OPEN_GL_TEXTURE_TYPE GL_UNSIGNED_INT_8_8_8_8
#endif


#if defined (__linux__)

#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_mixer.h"
#include "SDL/SDL_keysym.h"
#define True_Type_Font_Location "/usr/share/fonts/truetype/"   //linux
#define True_Type_Font_Defalut  "freefont/FreeSans.ttf" //linux
#define EE_OPEN_GL_TEXTURE_TYPE GL_UNSIGNED_BYTE
#endif





#define Type_Image 1
#define Type_TextBox 2
#define Type_Button 3
#define Type_ButtonTB 4
#define Type_Animation 5
#define Type_AnimationScript 6

//user defined events
#define ANIMATION_END_EVENT 28
#define ANIMATION_EVENT 29
#define TEXT_BOX_CHANGED_EVENT 30
#define NETWORK_DATA_RECIVED 50

//#define Anim_Funtion_Rotate 1
//#define Anim_Funtion_Motion 2

//error defintions
#define EPEE_NO_ERROR 0
#define TEXTBOX_CONVERION_ERROR 1
#define SOUND_PLAYING_ERROR 10
#define SOUND_ERROR         11
#define SOUND_LOADING_ERROR 12
#define SOUND_DELETING_ERROR 13
#define SOUND_NOT_FOUND      14
#define NULL_POINTER_PASSED 20
#define SPROCKET_NOT_FOUND 30
#define SPROCKET_ERROR     31
#define SPROCKET_DELETING_ERROR 32
#define RENDERLIST_NOT_FOUND 40
#define RENDERLIST_DELETING_ERROR 41
#define RENDERING_ERROR 50

//textbox Justifications
#define TEXT_LEFT_JUSTIFIED 100
#define TEXT_RIGHT_JUSTIFIED 102
#define TEXT_CENTER_JUSTIFIED 103

#define FONT_QUALITY_LOW 0
#define FONT_QUALITY_HIGH 1
#define FONT_QUALITY_SHADED 2

#define RECIVEING_IP 0
#define SENDING_IP 1




#endif // EPEEENGINECOMMENINCLUDES_H
