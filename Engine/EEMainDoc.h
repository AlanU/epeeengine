#ifndef EEMAINDOC_H
#define EEMAINDOC_H

/**\mainpage
 *The Epee Engine is a easy to use cross platform graphics engine with beginner programmers in mind.<br>
 *The Epee Engine tries to help new programmers by providing a logical, easy to use API for fast and easy set up.<br>
 *<br>
 *Some of my goal for the graphics engine are as follows:
 * Provide a graphics engine that any one can use.
 * To provide a graphics engine that supports mac programmers and other non-mainstream operating systems programmers.
 * A graphics engine that can be easily ported to consoles for homebrew development.
 *
 *
 * Examples on how to get started
 *
 *Lesson One: Hello World this lesson teaches how to create a new window and render text to the screen<br>
 *\code{.cpp}
 *int main (int argc, char *argv[ ])
 *{
 *  EpeeEngine GraphicsEngine;
 *   GraphicsEngine.SetUp(800,600,false,"Hello World",false,300,32);
 *  GraphicsEngine.CreateTextBox("hello",20,50,3,"Hello World","Current",-1,-1);
 *  while (1)
 *  {
 *      GraphicsEngine.RenderSeen(true);
 *  }
 *  return 0;
 *}
 *\endcode
 *After adding EpeeEngine.cpp and EpeeEngine.h to your project create a new file that will be the main file of your program.<br>
 *Inside this file add the following line at the top #include "EpeeEngine.h" this line will include all that you need to use the Epee Engine.<br>
 *First we are going to create a window and start the rendering cycle.<br>
 *<br>
 *\code{cpp}
 *int main (int argc, char *argv[ ])
 *{
 *   EpeeEngine GraphicsEngine;
 *\endcode
 *<br>
 *Here we create a new instance of the Epee Engine<br>
 *<br>
 * \code{cpp}
 *GraphicsEngine.SetUp(800,600,false,"Hello World",false,300,32);
 *\endcode
 *<br>
 *This initlizes the Epee Engine and creates a new window only the first two arguments are necessary. See the documentations for SetUp for more info about the arguments<br>
 *<br>
 *\code{cpp}
 *GraphicsEngine.CreateTextBox("hello",20,50,3,"Hello World","Current",-1,-1);
 *\endcode
 *This creates a new text box named hello that uses the a default font, displays it at position X 20 and Y 50, sets the hight and width of the text box to the size of the string Hello World, and adds it to the current render list.<br>
 *See CreateTextBox on how to change the color and font/font location.<br>
 *The default fonts and locations are as follows Mac: font Chalkboard.ttf font location /Library/Fonts/<br>
 *Windows: font ARIAL.TTF font location C:\\WINDOWS\\Fonts there are no defaults for Linux<br>
 *Linux users will need to change the font and font location to do this make this call instead<br>
 *\code{cpp}
 *GraphicsEngine.CreateTextBox("hello",20,50,3,"Hello World","Current",-1,-1,"truetypefont.ttf",18,255,255,255,"/fonts/");
 *\endcode
 *Where "truetypefont.ttf" is the name of a true type font file and /font/ is a location of that file. The argument after "truetypefont.ttf" is the font size.<br>
 *
 *\code{cpp}
 *while (1)
 *{
 *   GraphicsEngine.RenderSeen(true);
 *}
 * return 0;
 *}
 *\endcode
 *
 *This draws the textbox that we created to the screen and passing in true update the screen. See RenderSeen for more information<br>
 *Ok, we are done make sure you run this code in debug mode. If you do not see the words hello world in a new window look in the error file stderr.txt located in your project folder.<br>
 *This file records all errors reported by the epee engine there is also a debug file called stdout.txt located in the same directory as stderr.txt, which contains debug info.<br>
 *This code will display a new window and display "Hello World" in a new text box it.<br>
 **/



#endif // EEMAINDOC_H
