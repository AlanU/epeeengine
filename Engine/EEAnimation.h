/*
*  EEAnimation.h
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#ifndef EEANIMATION_H
#define EEANIMATION_H

#include "EpeeEngineCommenIncludes.h"
#include "EETexture.h"
#include "EEImage.h"
#include <vector>
//***********************Begin of Animation Class***************************

class Animation : public image
{
private:
	int m_ianimationFPS;
	float m_iFrameEverInMillSecounds;
	int m_iStripNumFrames;
	int m_iNumberOfRows;
	unsigned int m_iCurrentFrame;
	int m_iLoop;
	double m_dCurrentTime;
	float m_iPrevTime;
	bool m_bPlaying;
	int m_iNumberofTimesPlayed;
	bool m_bReversPlay;
	int m_iframeEvent;
	unsigned int WidthForBoxInPixles;
	unsigned int HeightForBoxInPixles;
	std::vector <EETexture> m_vTextureFrameList;
	int m_iEventID;

public:
	Animation(const std::string & _FileName,const std::string & _AnimationName,int _fps,int _x,int _y, int _z,int _AnimStripFrames,int _StartFrame=0,int _Loop=1,bool _Transparency=false,bool _StartNow=true, int _NumberOfRows=1);//:image( _FileName, _AnimationName, _x, _y, _z,Type_Animation,_Transparency)
	void  JumpToFrame(int _Frame);
	void UpdateTime();
	int GetFPS();
	void SetFPS(int _fps);
	void SetClipingBoxForFrame();
	void StopAnimation();
	void PlayAnimation();
	void PlayAnimation(int _frame);
	void ResumeAnimation();
	void SetLoop(int _Loop);
	int GetLoop();
	int GetCurrentFrame();
	bool GetReversPlay();
	void SetReversPlay(bool _BackWards);
	unsigned int GetTotalNumberOfFrames();
	int GetTotalNumberOfRows();
	int GetTotalNumberOfFramesPerRow();
	int GetNumberOfTimesPlayed();
	void SetFrameEvent(int _frame, int _eventID);
	int GetFrameEvent();
	int GetFrameEventID();
	bool IsPlaying();
	virtual SDL_Surface *  LoadSurface();
	virtual void UnLoadTexture();
	virtual bool IsLoaded();
	virtual void Init();
	~Animation();
	virtual void LoadTexture();
private:
	void CheckForNextFrame(float _time)	;
protected:
	virtual void CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image);
	virtual void  SDL_GL_SurfaceToTexture(SDL_Surface * surface);
	virtual void Render();

};

//********************End of Animation Class*************************************


#endif // EEANIMATION_H
