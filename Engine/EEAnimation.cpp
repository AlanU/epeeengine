/*
*  EEAnimation.cpp
*
*
*  Epee Engine
*  Created by Alan Uthoff on 3/12/06.
Copyright (C) 2011

This Code is free software; you can redistribute it and/or
modify it under the terms of the zlib/libpng License as published
by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.
This software is provided 'as-is', without any express or implied warranty.

In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.


*/

#include "EEAnimation.h"
#include "EELog.h"
//*************************Begin of Animation Class Functions**************************
Animation::Animation(const  std::string & _FileName, const std::string & _AnimationName,int _fps,int _x,int _y, int _z,int _AnimStripFrames,int _StartFrame,int _Loop,bool _Transparency,bool _StartNow, int _NumberOfRows):image( _FileName, _AnimationName, _x, _y, _z,Type_Animation,_Transparency)
{
	m_ianimationFPS=_fps;
	m_iEventID = 0;
	m_iFrameEverInMillSecounds=1000.00f/(float)m_ianimationFPS;

	if(_AnimStripFrames>0)
	{
		m_iStripNumFrames=(_AnimStripFrames-1);
	}
	else
	{
		m_iStripNumFrames=0;
	}

	if(_NumberOfRows<0)
	{
		m_iNumberOfRows=1;
	}
	else
	{
		m_iNumberOfRows=_NumberOfRows;
	}

	if (_Loop>=0) 
	{
		m_iLoop=_Loop;
	}
	else
	{
		m_iLoop=1;
	}

	m_iPrevTime=0;
	m_dCurrentTime=SDL_GetTicks();
	m_bPlaying=_StartNow;
	JumpToFrame(_StartFrame);
	m_iNumberofTimesPlayed=0;
	m_bReversPlay=false;
	m_iframeEvent=-1;
	this->SetBlit(true);
	/*	EETexture _TempTexture;
	for(int r=0;r<(GetTotalNumberOfFrames()+1);r++)
	{
	m_vTextureFrameList.push_back(_TempTexture);
	}*/


}

Animation::~Animation()
{
	/*for(int r=0;r<m_vTextureFrameList.size();r++)
	{
	glDeleteTextures( 1, &m_vTextureFrameList[r].m_GLTexture);
	}*/
	m_vTextureFrameList.clear();
}

void Animation::Init()
{
	this->JumpToFrame(m_iCurrentFrame);
	if(m_porignalSurface)
	{
		if((m_vTextureFrameList.size()>=m_iCurrentFrame) && (m_vTextureFrameList[m_iCurrentFrame].m_GLTexture==0) )
		{
			SDL_GL_SurfaceToTexture(m_porignalSurface);
		}
	}
	image::Init();
	return;
}

void  Animation::JumpToFrame(int _Frame)
{

	if (_Frame>=0 && _Frame<=(  ((m_iStripNumFrames+1)*m_iNumberOfRows))-1  ) {
		m_iCurrentFrame=_Frame;
		SetClipingBoxForFrame();
		if(m_porignalSurface)
		{
			if((m_vTextureFrameList.size()>=m_iCurrentFrame) && (m_vTextureFrameList[m_iCurrentFrame].m_GLTexture==0) )
			{
				SDL_GL_SurfaceToTexture(m_porignalSurface);
			}
		}
	}

}
SDL_Surface *  Animation::LoadSurface()
{

	return image::LoadSurface();
}
void Animation::UpdateTime()
{

	double time=(double)SDL_GetTicks()-m_dCurrentTime;
	int _currentframe=m_iCurrentFrame;
	CheckForNextFrame((float)time);
	//cout<<"Animation Upate Current Frame "<<m_iCurrentFrame<<" Time Diffrence "<<m_iCurrentTime-m_iPrevTime<<" frame pre millsecound "<<m_iFrameEverInMillSecounds<<std::endl;
	SetClipingBoxForFrame();
	if(m_iCurrentFrame!=_currentframe )
	{
		if((m_vTextureFrameList.size()>m_iCurrentFrame)  )
		{
			if(m_vTextureFrameList[m_iCurrentFrame].m_GLTexture==0)
			{
				if(m_porignalSurface)
				{
					SDL_GL_SurfaceToTexture(m_porignalSurface);
				}
			}
		}

	}

}

int Animation::GetFPS()
{
	return m_ianimationFPS;
}

void Animation::SetFPS(int _fps)
{
	m_ianimationFPS=_fps;
	m_iFrameEverInMillSecounds=1000.00f/(float)m_ianimationFPS;
	UpdateTime();

}
void  Animation::LoadTexture()
{
	int tempframe=m_iCurrentFrame;
	if(m_porignalSurface)
	{

		for(unsigned int y=0;y<=(GetTotalNumberOfFrames());y++)
		{
			m_iCurrentFrame=y;
			SetClipingBoxForFrame();
			SDL_GL_SurfaceToTexture(m_porignalSurface);

		}
		m_iCurrentFrame=tempframe;
		SDL_FreeSurface(m_porignalSurface);
		m_porignalSurface=NULL;
	}
}

void  Animation::SDL_GL_SurfaceToTexture(SDL_Surface * surface)
{



	/*if(m_GLTexture!=0)
	{
	glDeleteTextures( 1, &m_GLTexture);
	m_iTextureH=0;
	m_iTextureW=0;
	m_GLTexture=0;

	}*/

	if(ClipingRect.h==0 || ClipingRect.w==0)
	{
		SetClipingBoxForFrame();
	}
	int h=NearestPowerOfTwo(ClipingRect.h);
	int w=NearestPowerOfTwo(ClipingRect.w);

	SDL_Surface *image;
	SDL_Rect area;
	Uint32 saved_flags;
	Uint8  saved_alpha;
	image =





		SDL_CreateRGBSurface(
		SDL_SWSURFACE,
		w, h,
		32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
		0x000000FF,
		0x0000FF00,
		0x00FF0000,
		0xFF000000
#else
		0xFF000000,
		0x00FF0000,
		0x0000FF00,
		0x000000FF
#endif
		);

	if(m_pModifySurface)
	{
		SDL_FreeSurface(m_pModifySurface);
		m_pModifySurface=NULL;
	}

	saved_flags = surface->flags&(SDL_SRCALPHA|SDL_RLEACCELOK);
	saved_alpha = surface->format->alpha;
	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, 0, 0);
	}


	area.x = ClipingRect.x;
	area.y = ClipingRect.y;
	area.w = ClipingRect.w;
	area.h = ClipingRect.h;
	//SDL_FillRect(image,NULL,SDL_MapRGBA(image->format,0,255,0,255));
	SDL_BlitSurface(surface, &area, image, NULL);


	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, saved_flags, saved_alpha);
	}


	//m_pModifySurface=rotozoomSurfaceXY(	tempalpha,270,1,-1,0);
	CreateTexture(image,surface);

	//SDL_UnlockSurface(temp);
	SDL_FreeSurface(image);

	//SDL_FreeSurface(tempalpha);
	//return texture;
}


void Animation::CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image)
{
	GLuint _TempTexture=0;
	if(ClipingRect.h==0 || ClipingRect.w==0)
	{
		SetClipingBoxForFrame();
	}
	int h=NearestPowerOfTwo(ClipingRect.h);
	int w=NearestPowerOfTwo(ClipingRect.w);
	if(_baseSurface && _image)
	{
		glGenTextures(1, &_TempTexture);
		glBindTexture(GL_TEXTURE_2D, _TempTexture);
		//SDL_LockSurface(temp);


		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,  GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_R,GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, _baseSurface->pixels);
		//	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,_image->w,_image->h,GL_RGBA,GL_UNSIGNED_BYTE,_image->pixels);
		//m_iTextureH=h;
		//m_iTextureW=w;_TempTexture
		this->SetBlit(false);

		if(m_vTextureFrameList.size()<=GetTotalNumberOfFrames())
		{
			EETexture _TempTexture;
			while(m_vTextureFrameList.size()<=GetTotalNumberOfFrames())
			{
				m_vTextureFrameList.push_back(_TempTexture);
			}
		}
		if(m_vTextureFrameList[m_iCurrentFrame].m_GLTexture!=0)
		{
			glDeleteTextures( 1, &m_vTextureFrameList[m_iCurrentFrame].m_GLTexture);
			m_vTextureFrameList[m_iCurrentFrame].m_iTextureH=0;
			m_vTextureFrameList[m_iCurrentFrame].m_iTextureW=0;
			m_vTextureFrameList[m_iCurrentFrame].m_GLTexture=0;
			m_vTextureFrameList[m_iCurrentFrame].m_iClippingH=0;
			m_vTextureFrameList[m_iCurrentFrame].m_iClippingW=0;
		}
		m_vTextureFrameList[m_iCurrentFrame].m_GLTexture=_TempTexture;
		m_vTextureFrameList[m_iCurrentFrame].m_iTextureH=h;
		m_vTextureFrameList[m_iCurrentFrame].m_iTextureW=w;
		m_vTextureFrameList[m_iCurrentFrame].m_iClippingH=ClipingRect.h;
		m_vTextureFrameList[m_iCurrentFrame].m_iClippingW=ClipingRect.w;
	}
	return;
}
void Animation::UnLoadTexture()
{
	/* for(int r=0;r<m_vTextureFrameList.size();r++)
	{
	glDeleteTextures( 1, &m_vTextureFrameList[r].m_GLTexture);
	}*/
	m_vTextureFrameList.clear();
	/*EETexture _TempTexture;
	for(int r=0;r<(GetTotalNumberOfFrames()+1);r++)
	{
	m_vTextureFrameList.push_back(_TempTexture);
	}*/
	image::UnLoadTexture();

}

bool Animation::IsLoaded()
{
	if(m_vTextureFrameList.size()>GetTotalNumberOfFrames() && (m_vTextureFrameList[m_iCurrentFrame].m_GLTexture!=0))
	{
		return true;
	}

	return false;
}

void Animation::Render()
{
	if((m_vTextureFrameList.size()>m_iCurrentFrame) && (m_vTextureFrameList[m_iCurrentFrame].m_GLTexture!=0) )
	{
		m_iTextureH=m_vTextureFrameList[m_iCurrentFrame].m_iTextureH;
		m_iTextureW=m_vTextureFrameList[m_iCurrentFrame].m_iTextureW;
		this->SetHeightWidth(m_vTextureFrameList[m_iCurrentFrame].m_iClippingH,m_vTextureFrameList[m_iCurrentFrame].m_iClippingW);
		m_GLTexture=m_vTextureFrameList[m_iCurrentFrame].m_GLTexture;
		image::Render();
	}

}
/*
void Animation::CreateTexture(SDL_Surface * _baseSurface,SDL_Surface * _image)
{
int h=NearestPowerOfTwo(_image->h);
int w=NearestPowerOfTwo(_image->w);
if(_baseSurface && _image)
{
glGenTextures(1, &m_GLTexture);
glBindTexture(GL_TEXTURE_2D, m_GLTexture);
//SDL_LockSurface(temp);

//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT ) ;
//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT ) ;

//SDL_BlitSurface(_baseSurface,NULL,_image,NULL);
glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, _baseSurface->pixels);

//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) ;
//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) ;
//	glTexSubImage2D(GL_TEXTURE_2D,0,0,0,20,20,GL_BGRA,GL_UNSIGNED_BYTE,_image->pixels);
m_iTextureH=h;
m_iTextureW=w;

//image is fliped need to apply cliping that way
}
return;
}*/

void Animation::SetClipingBoxForFrame()
{

	if(m_porignalSurface)
	{
		int CurrentRow=0;
		if(((m_iCurrentFrame+1)%(m_iStripNumFrames+1)))
		{
			CurrentRow=((m_iCurrentFrame+1)/(m_iStripNumFrames+1));
		}
		else
		{
			CurrentRow=((m_iCurrentFrame+1)/(m_iStripNumFrames+1));
			CurrentRow--;
		}
		if( m_iNumberOfRows>1)
		{
			WidthForBoxInPixles=GetOrignalRect()->w/(m_iStripNumFrames+1);
		}
		else
		{
			WidthForBoxInPixles=GetOrignalRect()->w;
		}
		HeightForBoxInPixles=GetOrignalRect()->h/m_iNumberOfRows;
		int xpostion=0;
		if(CurrentRow!=0)
		{
			xpostion=WidthForBoxInPixles*((( (m_iCurrentFrame+1)-( CurrentRow*(m_iStripNumFrames+1) )))-1);
		}
		else
		{
			xpostion=WidthForBoxInPixles*m_iCurrentFrame;
		}
		SetClippingRect(xpostion,HeightForBoxInPixles*CurrentRow ,WidthForBoxInPixles,HeightForBoxInPixles);


	}


}

void Animation::StopAnimation()
{
	m_bPlaying=false;
}

void Animation::PlayAnimation()
{
	m_bPlaying=true;
	m_iNumberofTimesPlayed=0;
	JumpToFrame(0);

}
void Animation::PlayAnimation(int _frame)
{
	m_bPlaying=true;
	m_iNumberofTimesPlayed=0;
	JumpToFrame(_frame);

}
void Animation::ResumeAnimation()
{
	if(m_iNumberofTimesPlayed>=m_iLoop)
	{
		m_iNumberofTimesPlayed=0;
	}
	m_bPlaying=true;
}



void Animation::SetLoop(int _Loop)
{
	if (_Loop>=0)
	{
		m_iLoop=_Loop;
	}
	else
	{
		m_iLoop=1;
	}
}

int Animation::GetLoop()
{
	return m_iLoop;
}

int Animation::GetCurrentFrame()
{
	return m_iCurrentFrame;
}

bool Animation::GetReversPlay()
{
	return m_bReversPlay;
}

void Animation::SetReversPlay(bool _BackWards)
{
	m_bReversPlay=_BackWards;
}

unsigned int Animation::GetTotalNumberOfFrames()
{
	if(  ((m_iStripNumFrames+1)*m_iNumberOfRows)-1  )
	{
		EE_ERROR<<"Total Number of frames "<<((m_iStripNumFrames+1)*m_iNumberOfRows)-1<<"for : " <<this->GetName()<<" is less then zero"<<std::endl;
	}

	return ((m_iStripNumFrames+1)*m_iNumberOfRows)-1;
}

int Animation::GetTotalNumberOfRows()
{
	return m_iNumberOfRows;
}

int Animation::GetTotalNumberOfFramesPerRow()
{
	return m_iStripNumFrames;
}


int Animation::GetNumberOfTimesPlayed()
{
	return m_iNumberofTimesPlayed;
}

void Animation::SetFrameEvent(int _frame, int _eventID)
{
	m_iframeEvent=_frame;
	m_iEventID = _eventID;
}

int Animation::GetFrameEvent()
{
	return m_iframeEvent;
}

int Animation::GetFrameEventID()
{
	return m_iEventID;
}
bool Animation::IsPlaying()
{
	return m_bPlaying;
}

void Animation::CheckForNextFrame(float _time)
{
	if (_time>=m_iFrameEverInMillSecounds)
	{
		m_dCurrentTime=SDL_GetTicks();
		if ((m_iCurrentFrame<this->GetTotalNumberOfFrames() && m_bReversPlay==false)|| ( m_bReversPlay==true))
		{
			bool setevent=false;
			if (m_bPlaying)
			{
				if (m_bReversPlay)
				{
					m_iCurrentFrame--;

					if (m_iCurrentFrame==m_iframeEvent && m_iframeEvent!=-1) {
						setevent=true;
					}
					else
					{
						setevent=false;
					}

				}
				else
				{
					m_iCurrentFrame++;

					//** c.speer 11/6/07: changed >= to == to just throw one event.
					if (m_iCurrentFrame==m_iframeEvent && m_iframeEvent!=-1)
					{
						setevent=true;
					}
					else
					{
						setevent=false;
					}
				}

				if (setevent && m_bPlaying)
				{
					SDL_Event event;

					event.type = SDL_USEREVENT;
					event.user.code = ANIMATION_EVENT;
					event.user.data1 = this;
					event.user.data2 = 0;
					SDL_PushEvent(&event);
					//m_iframeEvent=-1;

				}



			}

		}
		else
		{
			if (m_bPlaying)
			{
				m_iNumberofTimesPlayed++;
				if (m_iNumberofTimesPlayed>=m_iLoop && m_iLoop!=0)
				{
					//** Throw an "End of Anim" event
					SDL_Event event;

					event.type = SDL_USEREVENT;
					event.user.code = ANIMATION_END_EVENT;
					event.user.data1 = this;
					event.user.data2 = 0;
					SDL_PushEvent(&event);
					StopAnimation();
				}
				else
				{

					if (m_bReversPlay)
					{
						m_iCurrentFrame=this->GetTotalNumberOfFrames();
					}
					else
					{

						m_iCurrentFrame=0;
					}

				}


			}

		}
		SetClipingBoxForFrame();

	}



}




//************************End of Animanimation Class Funtions*************************
